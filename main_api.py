from particle_tracker.api import SPTApi

if __name__ == "__main__":
    api = SPTApi(config="spt.config")
    api.registerFiles(".data")
    print(api.registeredFiles[0])
    print(api.registeredFilePaths[0])
    api.loadFile(0)
    pos = api.apiLocateParticles()
    print(pos.head(5))

    tracks = api.apiTrackParticles()
    print(tracks.head(5))

    tracks = api.apiIdentifyAndTrackParticles()
    print(tracks.head(5))
    
    api.apiAnalyseTracks()
    api.apiExportAllPlots()
    api.apiExportAllData()
    print(0)
