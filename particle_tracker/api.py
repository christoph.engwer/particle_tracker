import os
from PyQt5 import Qt, QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from .core import SingleParticleTracker
from .defines import IdentificationMethod

class SPTApi(SingleParticleTracker):
    def __init__(self, config=None, parent=None):
        self.app = QtWidgets.QApplication([])
        super().__init__([], config=config, parent=parent)

    def loadDefaultData(self):
        self.loadData.clicked.emit()
    
    def registerFiles(self, path):
        path = os.path.abspath(path) + "/"
        self.addFileToList(path)

    @property
    def registeredFiles(self):
        files = self.fileList.findItems("", Qt.Qt.MatchContains, 0)
        return [f.text(0) for f in files]

    @property
    def registeredFilePaths(self):
        files = self.fileList.findItems("", Qt.Qt.MatchContains, 0)
        return [f.text(1) for f in files]

    def loadFile(self, index):
        item = self.fileList.topLevelItem(index)
        if item is None:
            raise IndexError("File is not registered")
        # self.fileList.setCurrentItem(item)
        self.fileSelected(item)

    def apiSetDetectionMode(self, mode):
        if mode == IdentificationMethod.gaussianBlob:
            self.detectionMode.setCurrentIndex(0)
        elif mode == IdentificationMethod.watershed:
            self.detectionMode.setCurrentIndex(1)
        else:
            raise IndexError("Mode not valid")
    
    def apiSetParticleSize(self, size:int): 
        self.sizeSpinBox.setValue(int(size))
    
    def apiSetThreshold(self, threshold:int):
        self.thresholdSpinBox.setValue(int(threshold))

    def apiLoadConfigFile(self, config):
        self.loadConfig(config)

    def apiLocateParticles(self):
        loop = QtCore.QEventLoop()
        self.loadingTracksDone.connect(loop.quit)
        self.identifyButton.clicked.emit()
        loop.exec()
        loop.exit(0)
        return self.pos

    def apiTrackParticles(self):
        self.trackButton.clicked.emit()
        return self.tracks

    def apiIdentifyAndTrackParticles(self):
        loop = QtCore.QEventLoop()
        self.loadingTracksDone.connect(loop.quit)
        self.trackAndIdentifyButton.clicked.emit()
        loop.exec()
        loop.exit(0)
        return self.tracks

    def apiAnalyseTracks(self):
        self.updateTrackAnalysis.clicked.emit()

    def apiExportAllPlots(self):
        self.saveAllButton.clicked.emit()
    
    def apiExportAllData(self):
        self.saveDataButton.clicked.emit()
