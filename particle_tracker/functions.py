#! /usr/bin/python3

import logging
import multiprocessing as mp
import subprocess
import sys
from itertools import repeat

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import trackpy as tp
from PyQt5 import QtCore, QtGui
from scipy import ndimage
from scipy import ndimage as ndi
from scipy import stats
from skimage import transform
from skimage.feature import canny, peak_local_max
from skimage.filters import threshold_local, threshold_otsu, try_all_threshold
from skimage.io import imread, imsave
from skimage.segmentation import watershed

from .defines import thresholds


def myCallback(data):
    # print(data)
    res, i = data
    logger = logging.getLogger()
    logger.warn("downsampling: %s", (i))
    # print(i)
    return res

def myTransform(im, shape):
    # logger = mp.get_logger()
    # logger = logging.getLogger()
    # logger.warn("downsampling: %s", (i))
    res = transform.resize(im, shape)
    return res

def myWatershed(image, imProcess, thresholder, distProcess, i=None):
    if i is None:
        i = 0
    image = ndi.gaussian_filter(image, imProcess)
    method = thresholds[thresholder]
    y, x = image.shape
    if thresholder == "Local":
        # make window smaller by n = 5 with respect to image dimensions, then round to nearest odd number
        window = ((x + y) / 2 / 5) // 2 * 2 + 1
        cutoff = method(image, window)
    else:
        cutoff = method(image)
    image = image > cutoff

    distance = ndi.distance_transform_edt(image)
    distance = ndi.gaussian_filter(distance, distProcess)
    local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3, 3)), labels=image)
    markers = ndi.label(local_maxi)[0]
    labels = watershed(-distance, markers, mask=image)

    n = labels.max()
    xpos = np.zeros(n)
    ypos = np.zeros(n)
    size = np.zeros(n)
    edges = list()
    for j in range(n):
        # determine center of particle
        yi, xi = np.where(labels == j + 1)
        xmean = xi.mean()
        ymean = yi.mean()
        xpos[j] = xmean
        ypos[j]  = ymean

        # size of particle
        center = np.array((xmean, ymean))
        pos = np.array((xi, yi))
        dist = pos - center[:, None]
        d = np.linalg.norm(dist, axis=0)
        size[j] = d.mean()

        # edge coordinates
        edge = canny((labels == j + 1).astype(int), low_threshold=1, high_threshold=1).astype(int)
        yedge, xedge = np.where(edge == 1)
        edgePoints = np.array((xedge, yedge)).T
        edges.append(edgePoints)
    data = {
        "y": ypos,
        "x": xpos,
        "size": size,
        "edge": edges,
        "frame": np.array([i]*n, dtype=int)
    }
    df = pd.DataFrame(data=data)

    return df

def myLocate(image, size, invert, threshold, sep, iterations):
    # print(image.mean())
    res = tp.locate(image, size, invert=invert, threshold=threshold, separation=sep, max_iterations=iterations)
    # print(res)
    return res

def myBatch(frames, size, threshold, sep, iterations):
    res = tp.batch(frames, size, invert=False, threshold=threshold, separation=sep, max_iterations=iterations, engine="numba", processes=mp.cpu_count())
    return res

def pixmapFromRGBImage(rgbIm, scaleIntensity=True, scaledSize=None):
    if scaleIntensity:
        vmin = rgbIm.min()
        rgbIm -= vmin
        vmax = rgbIm.max()
        rgbIm = rgbIm.astype(np.float) / vmax * 255
        rgbIm = rgbIm.astype(np.uint8)

    im_h, im_w, im_c = rgbIm.shape
    qim = QtGui.QImage(rgbIm.data, im_w, im_h, im_c*im_w, QtGui.QImage.Format_RGB888)
    pix = QtGui.QPixmap(qim)
    if scaledSize is not None:
        w = scaledSize[0]
        h = scaledSize[1]
        pix = pix.scaled(w, h, QtCore.Qt.KeepAspectRatio)
    return pix

def fit_powerlaw(data, plot=True, **kwargs):
    """Fit a powerlaw by doing a linear regression in log space."""
    ys = pd.DataFrame(data)
    x = pd.Series(data.index.values, index=data.index, dtype=np.float64)
    values = pd.DataFrame(index=['n', 'A'])
    fits = {}
    for col in ys:
        y = ys[col].dropna()
        xnew, ynew = np.array(list(zip(x, y))).T
        slope, intercept, r, p, stderr = stats.linregress(np.log(xnew), np.log(ynew))
        values[col] = [slope, np.exp(intercept)]
        fits[col] = x.apply(lambda x: np.exp(intercept)*x**slope)
    values = values.T
    fits = pd.concat(fits, axis=1)
    if plot:
        from trackpy import plots
        plots.fit(data, fits, logx=True, logy=True, legend=False, **kwargs)
    return values

def my_exception_hook(type, value, tb):
    """
    Intended to be assigned to sys.exception as a hook.
    Gives programmer opportunity to do something useful with info from uncaught exceptions.

    Parameters
    type: Exception type
    value: Exception's value
    tb: Exception's traceback
    """
    error_msg = "An exception has been raised outside of a try/except!!!\n" \
                f"Type: {type}\n" \
                f"Value: {value}\n" \
                f"Traceback: {tb}"
    # We're just printing the error out for this example, 
    # but you should do something useful here!
    # Log it! Email your boss! Tell your cat!
    print(error_msg)
    # raise type(value)

def ignore(var):
    return None

def fullRotateSingle(img, angle, pivot):
    pivot = pivot.astype(int)
    padX = [img.shape[1] - pivot[0], pivot[0]]
    padY = [img.shape[0] - pivot[1], pivot[1]]
    imgP = np.pad(img, [padY, padX], 'constant')
    # print(imgP.shape)
    # new_axes = (axes[0] - 1, axes[1] - 1)
    # pool = mp.Pool(mp.cpu_count())
    # imgR = pool.starmap(rotateSingle, zip(imgP, repeat(new_axes), repeat(angle)))
    # pool.close()
    # pool.join()
    # imgR = np.array(imgR)
    # imgR = ndimage.rotate(imgP, angle, axes=axes, reshape=False, mode="nearest")
    imgR = rotateSingle(imgP, (0, 1), angle)
    return imgR[padY[0] : -padY[1], padX[0] : -padX[1]]

def rotateSingle(im, ax, angle):
    return ndimage.rotate(im, angle, axes=ax, reshape=False, mode="nearest")

def rotateImage(img, angle, pivot, axes=(0, 1)):
    pivot = pivot.astype(int)
    padX = [img.shape[-1] - pivot[0], pivot[0]]
    padY = [img.shape[-2] - pivot[1], pivot[1]]
    imgP = np.pad(img, [[0, 0], padY, padX], 'constant')
    # print(imgP.shape)
    new_axes = (axes[0] - 1, axes[1] - 1)
    pool = mp.Pool(mp.cpu_count())
    imgR = pool.starmap(rotateSingle, zip(imgP, repeat(new_axes), repeat(angle)))
    pool.close()
    pool.join()
    imgR = np.array(imgR)
    # imgR = ndimage.rotate(imgP, angle, axes=axes, reshape=False, mode="nearest")
    return imgR[:, padY[0] : -padY[1], padX[0] : -padX[1]]

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    angle = np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))
    # angle = np.arccos(np.dot(v1_u, v2_u))
    # angle = np.math.atan2(np.linalg.det([v1, v2]), np.dot(v1, v2))
    return np.rad2deg(angle)

def angle_between_points(p0, p1, p2):
    v0 = np.array(p0) - np.array(p1)
    v1 = np.array(p2) - np.array(p1)

    angle = np.math.atan2(np.linalg.det([v0,v1]),np.dot(v0,v1))
    return np.degrees(angle)

def sigmoid(x, a, b, n):
    # a, b, n = para
    res = a * x**n/ (x**n + b**n)
    return res

def rotate(p, origin=(0, 0), degrees=0):
    angle = np.deg2rad(degrees)
    R = np.array([[np.cos(angle), -np.sin(angle)],
                  [np.sin(angle),  np.cos(angle)]])
    o = np.atleast_2d(origin)
    p = np.atleast_2d(p)
    return np.squeeze((R @ (p.T-o.T) + o.T).T)

def plot4Points(points):
    lower_left, upper_left, upper_right, lower_right = points
    plt.scatter(points[:, 0], points[:, 1], s=300)
    plt.show()

def sortPolygon(points, testplot=False, polygon=True):
    if len(points) > 4:
        return None
    sort = points[points[:, 1].argsort(axis=0)]
    lower_points = sort[:2, :]
    upper_points = sort[2:, :]

    lower_left = lower_points[lower_points[:, 0].argmin()]
    lower_right = lower_points[lower_points[:, 0].argmax()]
    upper_left = upper_points[upper_points[:, 0].argmin()]
    upper_right = upper_points[upper_points[:, 0].argmax()]

    if testplot:
        plt.scatter(points[:, 0], points[:, 1], s=300)
        plt.scatter(*lower_left, c="r")
        plt.scatter(*lower_right, c="g")
        plt.scatter(*upper_left, c="b")
        plt.show()
    
    if polygon:
        return np.array((lower_left, upper_left, upper_right, lower_right), dtype=int)
    else:
        return np.array(((lower_left, lower_right), (upper_left, upper_right)), dtype=int)

def scaleCellBoundaries(points, upper, lower):
    lower_left, upper_left, upper_right, lower_right = points

    left_direction = upper_left - lower_left
    left_steps = np.linspace(0, 1, 1000)
    left_points = lower_left[None, :] + left_steps[:, None] * left_direction[None, :]
    new_lower_left = left_points[np.argmin(np.abs(left_points[:, 1] - lower))]
    new_upper_left = left_points[np.argmin(np.abs(left_points[:, 1] - upper))]

    right_direction = upper_right - lower_right
    right_steps = np.linspace(0, 2, 1000)
    right_points = lower_right[None, :] + right_steps[:, None] * right_direction[None, :]
    new_lower_right = right_points[np.argmin(np.abs(right_points[:, 1] - lower))]
    new_upper_right = right_points[np.argmin(np.abs(right_points[:, 1] - upper))]

    return np.array((new_lower_left, new_upper_left, new_upper_right, new_lower_right), dtype=int)

if __name__ == "__main__":
    p = np.random.random((4, 2))
    p = sortPolygon(p, True)
    p = scaleCellBoundaries(p, 1, 0)
    plot4Points(p)
