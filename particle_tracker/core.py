#! /usr/bin/python3

import os
import subprocess
import sys
import warnings
import json

warnings.filterwarnings("ignore")
from time import sleep, time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pyqtgraph as pg
import tifffile
import trackpy as tp
from matplotlib import colors as mpl_colors
from PIL import Image, ImageDraw
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import pyqtSignal, pyqtSlot

# ignore default TrackPy logging output which will be handled individually
tp.ignore_logging()
import inspect

import cv2
import qdarkstyle
import seaborn as sns
from scipy import ndimage as ndi
from scipy.ndimage import gaussian_filter
from scipy.optimize import curve_fit
from scipy.signal import argrelextrema
from scipy.spatial import Voronoi
from skimage import transform
from skimage.feature import canny, peak_local_max
from skimage.io import imread, imsave

from .classes import *
from .defines import *
from .functions import *
from .smallestenclosingcircle import make_circle
from .flowlayout import FlowLayout

class SingleParticleTracker(QtWidgets.QMainWindow):
    """The main gui class for the Single Particle Tracker

    Load or drag/drop tif images to segment and track particles in grey scale
    images

    Args:
        args (list): list of system arguments, typically sys.argv
        parent (QWidget): The parent QWidget if any (Default: None)

    """
    # some signals to send data to different threads for reading/loading an
    # image, the progress bar etc
    localizeParticles = pyqtSignal(np.ndarray, dict)
    trackParticles = pyqtSignal(dict)
    sendStatus = pyqtSignal(str)
    sendProgress = pyqtSignal(int)
    sendImPath = pyqtSignal(str)
    sendPreview = pyqtSignal(np.ndarray)
    sendThresholdPreview = pyqtSignal(np.ndarray)
    sendFilteredTracks = pyqtSignal(pd.DataFrame, np.ndarray, np.ndarray, pd.DataFrame)
    loadingTracksDone = pyqtSignal()

    def __init__(self, args, config=None, parent=None):
        # Initialise the main class by calling the parent class
        super().__init__(parent)

        # setup all gui elements from the .ui file
        path = os.path.dirname(os.path.abspath(__file__))
        uic.loadUi(path + "/ui/main.ui", self)
        self.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())

        # load a config file if given
        if config is not None:
            self.loadConfig(config)

        # These items are re-assigned solely to simplify programming
        self.fileList = self.findChild(QtWidgets.QTreeWidget, "fileList")
        self.fileList.setColumnWidth(0, 250)
        self.fileList.setColumnWidth(1, 500)
        self.tabs = self.findChild(QtWidgets.QTabWidget, "tabs")
        self.loadData = self.findChild(QtWidgets.QPushButton, "loadData")
        self.loadFiles = self.findChild(QtWidgets.QPushButton, "loadFiles")
        self.maxZProjection = self.findChild(QtWidgets.QCheckBox, "maxZProjection")
        self.trackButton = self.findChild(QtWidgets.QPushButton, "trackButton")
        self.currentFileView = self.findChild(QtWidgets.QLabel, "currentFileView")
        self.dataTScroll = self.findChild(QtWidgets.QScrollBar, "dataTScroll")
        self.dataMinScroll = self.findChild(QtWidgets.QScrollBar, "dataMinScroll")
        self.dataMaxScroll = self.findChild(QtWidgets.QScrollBar, "dataMaxScroll")

        self.currentImageView = self.findChild(QtWidgets.QLabel, "currentImageView")
        self.colored = self.findChild(QtWidgets.QCheckBox, "colored")
        self.rotateButton = self.findChild(QtWidgets.QPushButton, "rotateButton")
        self.analyseButton = self.findChild(QtWidgets.QPushButton, "analyseButton")

        self.sizeSpinBox = self.findChild(QtWidgets.QSpinBox, "sizeSpinBox")
        self.thresholdSpinBox = self.findChild(QtWidgets.QSpinBox, "thresholdSpinBox")
        self.sepSpinBox = self.findChild(QtWidgets.QSpinBox, "sepSpinBox")
        self.iterSpinBox = self.findChild(QtWidgets.QSpinBox, "iterSpinBox")
        self.resizeSpinBox = self.findChild(QtWidgets.QSpinBox, "resizeSpinBox")
        self.downsamplePreviewButton = self.findChild(QtWidgets.QPushButton, "downsamplePreviewButton")
        self.identifyButton = self.findChild(QtWidgets.QPushButton, "identifyButton")
        self.searchSpinBox = self.findChild(QtWidgets.QSpinBox, "searchSpinBox")
        self.memorySpinBox = self.findChild(QtWidgets.QSpinBox, "memorySpinBox")
        self.voronoiBox = self.findChild(QtWidgets.QCheckBox, "voronoiBox")
        self.voronoiOptionBox = self.findChild(QtWidgets.QCheckBox, "voronoiOptionBox")
        self.trackingResult = self.findChild(QtWidgets.QLabel, "trackingResult")
        self.currentTrackingResult = self.findChild(QtWidgets.QLabel, "currentTrackingResult")

        self.trackView = self.findChild(QtWidgets.QWidget, "widget_000")
        self.analysisHist_1 = self.findChild(QtWidgets.QWidget, "widget_001")
        self.analysisHist_2 = self.findChild(QtWidgets.QWidget, "widget_002")
        self.analysisHist_3 = self.findChild(QtWidgets.QWidget, "widget_003")
        self.analysisHist_4 = self.findChild(QtWidgets.QWidget, "widget_004")
        self.analysisHist_5 = self.findChild(QtWidgets.QWidget, "widget_005")
        self.analysisHist_6 = self.findChild(QtWidgets.QWidget, "widget_006")
        self.analysisHist_7 = self.findChild(QtWidgets.QWidget, "widget_007")
        self.analysisHist_8 = self.findChild(QtWidgets.QWidget, "widget_008")
        self.balisticSpinBox = self.findChild(QtWidgets.QDoubleSpinBox, "balisticSpinBox")
        self.radialSpinBox = self.findChild(QtWidgets.QDoubleSpinBox, "radialSpinBox")
        self.lengthSpinBox = self.findChild(QtWidgets.QSpinBox, "lengthSpinBox")
        self.voxelSpinBox = self.findChild(QtWidgets.QDoubleSpinBox, "voxelSpinBox")
        self.timestepSpinBox = self.findChild(QtWidgets.QDoubleSpinBox, "timestepSpinBox")
        self.updateTrackAnalysis = self.findChild(QtWidgets.QPushButton, "updateTrackAnalysis")
        self.rescaleButton = self.findChild(QtWidgets.QPushButton, "rescaleButton")
        self.saveTrackButton = self.findChild(QtWidgets.QPushButton, "saveTrackButton")
        self.saveBalisticButton = self.findChild(QtWidgets.QPushButton, "saveBalisticButton")
        self.saveRadialButton = self.findChild(QtWidgets.QPushButton, "saveRadialButton")
        self.saveDistanceButton = self.findChild(QtWidgets.QPushButton, "saveDistanceButton")
        self.saveMSDButton = self.findChild(QtWidgets.QPushButton, "saveMSDButton")
        self.saveAllButton = self.findChild(QtWidgets.QPushButton, "saveAllButton")
        self.saveDataButton = self.findChild(QtWidgets.QPushButton, "saveDataButton")
        self.actionAbout = self.findChild(QtWidgets.QAction, "actionAbout")
        self.actionChangelog = self.findChild(QtWidgets.QAction, "actionChangelog")
        self.actionReadme = self.findChild(QtWidgets.QAction, "actionReadme")
        flowLayout = FlowLayout(orientation=QtCore.Qt.Horizontal)
        flowLayout.addWidget(self.trackView)
        flowLayout.addWidget(self.analysisHist_1)
        flowLayout.addWidget(self.analysisHist_2)
        flowLayout.addWidget(self.analysisHist_3)
        flowLayout.addWidget(self.analysisHist_4)
        flowLayout.addWidget(self.analysisHist_5)
        flowLayout.addWidget(self.analysisHist_6)
        flowLayout.addWidget(self.analysisHist_7)
        flowLayout.addWidget(self.analysisHist_8)
        self.scrollAreaWidgetContents.setLayout(flowLayout)

        # enable file dropping on gui
        self.setAcceptDrops(True)

        # initialise progress bar
        self.progressDialog = MyProgress(self)
        self.progressDialog.resetActionCanceled.connect(self.handledAccepted)
        self.progressDialog.accepted.connect(self.handledAccepted)
        self.progressDialog.rejected.connect(self.handleRejected)

        # initialise preview window
        self.previewWindow = MyPreview()

        self.imageReader = MyImageReader()
        self.tracker = ParticleTracker()
        self.analyser = TrackAnalyser()

        # connect all signals and slots from the various gui elements, signals
        # to all relevant slots
        self.imageReader.sendProgress.connect(self.progressDialog.updateProgress)
        self.fileList.itemDoubleClicked.connect(self.fileSelected)
        self.sendImPath.connect(self.imageReader.readImage)
        self.imageReader.sendImage.connect(self.processImage)
        self.loadData.clicked.connect(self.loadDataDir)
        self.loadFiles.clicked.connect(self.loadSelectedFiles)
        self.trackButton.clicked.connect(self.calcTracks)
        self.dataTScroll.valueChanged.connect(self.dataTimeChanged)
        self.dataZScroll.valueChanged.connect(self.dataTimeChanged)
        self.dataCScroll.valueChanged.connect(self.dataTimeChanged)
        self.dataMinScroll.valueChanged.connect(self.dataTimeChanged)
        self.dataMaxScroll.valueChanged.connect(self.dataTimeChanged)
        self.dataTValue.valueChanged.connect(self.dataTimeChanged)
        self.dataZValue.valueChanged.connect(self.dataTimeChanged)
        self.dataCValue.valueChanged.connect(self.dataTimeChanged)
        self.dataMinValue.valueChanged.connect(self.dataTimeChanged)
        self.dataMaxValue.valueChanged.connect(self.dataTimeChanged)
        self.colored.stateChanged.connect(self.dataTimeChanged)
        self.identifyButton.clicked.connect(self.calcTracks)
        self.trackParticles.connect(self.tracker.trackParticles)
        self.roiEdgesBox.stateChanged.connect(self.voronoiBoxChanged)
        self.voronoiBox.stateChanged.connect(self.voronoiBoxChanged)
        self.voronoiOptionBox.stateChanged.connect(self.voronoiBoxChanged)
        self.trackAndIdentifyButton.clicked.connect(self.calcTracks)
        self.localizeParticles.connect(self.tracker.identifyParticlesWrapper)
        self.tracker.sendTracks.connect(self.loadTrackData)
        self.tracker.sendTrackingError.connect(self.handleTrackingError)
        self.tracker.logger.sendProgress.connect(self.progressDialog.updateProgress)
        self.tracker.sendStatus.connect(self.progressDialog.updateStatus)
        self.tracker.sendProgress.connect(self.progressDialog.updateProgress)
        self.tracker.sendSize.connect(self.setTmax)
        self.sendProgress.connect(self.progressDialog.updateProgress)
        self.sendStatus.connect(self.progressDialog.updateStatus)
        self.updateTrackAnalysis.clicked.connect(self.analyseTracks)
        self.downsamplePreviewButton.clicked.connect(self.downsamplePreviewButtonClicked)
        self.sendPreview.connect(self.previewWindow.displayImage)
        self.thresholdPreviewButton.clicked.connect(self.thresholdPreviewClicked)
        self.sendFilteredTracks.connect(self.analyser.analyseTracks)
        self.analyser.sendStatus.connect(self.progressDialog.updateStatus)
        self.analyser.sendProgress.connect(self.progressDialog.updateProgress)
        self.analyser.sendTrackAnalysis.connect(self.plotTrackAnalysis)
        self.rescaleButton.clicked.connect(self.rescaleAnalysisPlots)
        self.saveTrackButton.clicked.connect(self.saveTrackButtonClicked)
        self.saveBalisticButton.clicked.connect(self.saveBalisticButtonClicked)
        self.saveRadialButton.clicked.connect(self.saveRadialButtonClicked)
        self.saveDistanceButton.clicked.connect(self.saveDistanceButtonClicked)
        self.saveMSDButton.clicked.connect(self.saveMSDButtonClicked)
        self.saveAllButton.clicked.connect(self.saveAllButtonClicked)
        self.saveDataButton.clicked.connect(self.saveTrackData)
        self.balisticSpinBox.valueChanged.connect(self.setAnalysisValueChanged)
        self.radialSpinBox.valueChanged.connect(self.setAnalysisValueChanged)
        self.lengthSpinBox.valueChanged.connect(self.setAnalysisValueChanged)
        self.voxelSpinBox.valueChanged.connect(self.setAnalysisValueChanged)
        self.timestepSpinBox.valueChanged.connect(self.setAnalysisValueChanged)

        self.actionAbout.triggered.connect(self.showAboutDialog)
        self.actionChangelog.triggered.connect(self.showChangelog)
        self.actionReadme.triggered.connect(self.showReadme)
        self.tabs.currentChanged.connect(self.tabChanged)

        self.voronoiOptionBox.setVisible(False)

        # initialise empty attributes for later
        self.im = None
        self.pos = None
        self.tracks = None
        self.com = None
        self.plotReady = False
        self.changelog = MarkdownViewer(self)
        self.readme = MarkdownViewer(self)
        self.actionCanceled = False
        self.analysisChanged = False

        self.installEventFilter(self)
        self.cursorLabel = QtWidgets.QLabel(self)
        self.cursorLabel.setVisible(False)
        self.cursorLabel.setStyleSheet("QLabel {background-color: rgba(1, 1, 1, 0); padding: 0px  }")

        self.tabs.setCurrentWidget(self.tabLoad)
        self.setWindowState(QtCore.Qt.WindowMaximized)
    
    def loadConfig(self, path):
        with open(path, 'r') as json_file:
            config = json.load(json_file)
        try:
            self.resizeSpinBox.setValue(config["downsampling"])
        except (KeyError, TypeError):
            pass
        try:
            self.sizeSpinBox.setValue(config["gaussianBlobDetection"]["size"])
        except (KeyError, TypeError):
            pass
        try:
            self.thresholdSpinBox.setValue(config["gaussianBlobDetection"]["threshold"])
        except (KeyError, TypeError):
            pass
        try:
            self.sepSpinBox.setValue(config["gaussianBlobDetection"]["separation"])
        except (KeyError, TypeError):
            pass
        try:
            self.iterSpinBox.setValue(config["gaussianBlobDetection"]["iterations"])
        except (KeyError, TypeError):
            pass
        try:
            self.thresholdMethod.setCurrentText(config["watershedDetection"]["thresholder"])
        except (KeyError, TypeError):
            pass
        try:
            self.imageProcessSpinBox.setValue(config["watershedDetection"]["imageProcess"])
        except (KeyError, TypeError):
            pass
        try:
            self.distanceProcessSpinBox.setValue(config["gaussianBlobDetection"]["distanceProcess"])
        except (KeyError, TypeError):
            pass
        try:
            self.maxZProjection.setChecked(config["maxZProjection"])
        except (KeyError, TypeError):
            pass
        try:
            self.searchSpinBox.setValue(config["tracking"]["searchRange"])
        except (KeyError, TypeError):
            pass
        try:
            self.memorySpinBox.setValue(config["tracking"]["memory"])
        except (KeyError, TypeError):
            pass
        try:
            self.balisticSpinBox.setValue(config["analysis"]["balistic"])
        except (KeyError, TypeError):
            pass
        try:
            self.radialSpinBox.setValue(config["analysis"]["radial"])
        except (KeyError, TypeError):
            pass
        try:
            self.lengthSpinBox.setValue(config["analysis"]["length"])
        except (KeyError, TypeError):
            pass
        try:
            self.voxelSpinBox.setValue(config["analysis"]["voxel"])
        except (KeyError, TypeError):
            pass
        try:
            self.timestepSpinBox.setValue(config["analysis"]["timestep"])
        except (KeyError, TypeError):
            pass

    def handleRejected(self):
        """Slot is called when the progress dialog has been canceled.
        """
        self.actionCanceled = True
        self.imageReader.actionCanceled = True
        self.tracker.actionCanceled = True
        self.analyser.actionCanceled = True
        self.tracker.logger.actionCanceled = True

    def handledAccepted(self):
        """Slot is called when the progress dialog finished successfully.
        """
        self.actionCanceled = False
        self.imageReader.actionCanceled = False
        self.tracker.actionCanceled = False
        self.analyser.actionCanceled = False
        self.tracker.logger.actionCanceled = False

    def eventFilter(self, o, e):
        """
        Draw a circle at the cursor when clicking on the tracked particles
        
        Arguments:
            o {QtCore.QObject} -- QObject triggering the event filter
            e {QtCore.QEvent} -- QEvent type
        
        Returns:
            bool -- False
        """
        if e.type() == QtCore.QEvent.MouseMove or e.type() == QtCore.QEvent.MouseButtonPress:
            w = self.currentFileView.width()
            h = self.currentFileView.height()
            pos = QtGui.QCursor.pos()
            pos = self.currentFileView.mapFromGlobal(pos)
            x = pos.x()
            y = pos.y()
            if self.tabs.currentWidget() == self.tabLoad and self.currentFileView.pixmap() is not None and x >= 0 and y >= 0 and x <= w and y <= h:
                pix_size = np.array((self.currentFileView.pixmap().height(), self.currentFileView.pixmap().width()))
                real_size = self.im.shape[-2:]
                scale = np.mean(pix_size / real_size)
                s = self.sizeSpinBox.value() * scale
                qmask = QtGui.QImage(s, s, QtGui.QImage.Format_ARGB32)
                qmask.fill(QtCore.Qt.transparent)
                painter = QtGui.QPainter(qmask)
                painter.setPen(QtCore.Qt.red)
                painter.setRenderHint(QtGui.QPainter.Antialiasing, True)
                painter.drawEllipse(1, 1, s-1, s-1)
                painter.end()
                pm = QtGui.QPixmap.fromImage(qmask)
                self.cursorLabel.setPixmap(pm)
                rect = self.currentFileView.geometry()
                x += rect.x() + 2 - s/2 + 1
                y += rect.y() + 22 - s/2 + 1
                self.cursorLabel.setVisible(True)
                self.cursorLabel.setGeometry(x, y, s, s)
            else:
                self.cursorLabel.setVisible(False)
        if e.type() == QtCore.QEvent.MouseButtonRelease:
            self.cursorLabel.setVisible(False)
        return False

    def showAboutDialog(self):
        """Show Single Particle Tracker information
        """
        about = AboutDialog(self)
        about.show()
    
    def showChangelog(self):
        """Show changelog by version
        """
        self.changelog.path = "CHANGELOG"
        self.changelog.show()
    
    def showReadme(self):
        """Show readme file
        """
        self.readme.path = "README.md"
        self.readme.show()

    def thresholdPreviewClicked(self):
        """Test all possible thresholder on the current image. Scales the image
        according to the downsampling ratio.
        """
        if self.im is None:
            return None
        self.thresholdPreviewWindow = ThresholdPreview()
        self.sendThresholdPreview.connect(self.thresholdPreviewWindow.showThresholds)
        self.thresholdPreviewWindow.sendThresholder.connect(self.thresholdMethod.setCurrentIndex)
        t = self.dataTScroll.value()
        z = self.dataZScroll.value()
        c = self.dataCScroll.value()
        if self.maxZProjection.isChecked():
            im = self.im[t, :, c, :, :].max(axis=0)
        else:
            im = self.im[t, z, c, :, :]*1
        resize = self.resizeSpinBox.value()
        shape = np.array(im.shape)
        new_shape = (shape * resize/100.0).astype(int)
        scaled = transform.resize(im, new_shape).astype(np.float)
        imProcess = self.imageProcessSpinBox.value()
        scaled = ndi.gaussian_filter(scaled, imProcess)
        self.sendThresholdPreview.emit(scaled)
        self.thresholdPreviewWindow.show()

    def downsamplePreviewButtonClicked(self):
        """
        Take the currently display image and apply the downsampling. The
        downsampled image will be emitted to and displayed on a preview window.
        """
        if self.im is None:
            return
        t = self.dataTScroll.value()
        c = self.dataCScroll.value()
        if self.maxZProjection.isChecked():
            im = self.im[t, :, c, :, :].max(axis=0)
        else:
            z = self.dataZScroll.value()
            im = self.im[t, z, c, :, :]*1
        vmin = self.dataMinScroll.value()
        vmax = self.dataMaxScroll.value()
        im[im > vmax] = vmax
        im -= vmin
        im[im < 0] = 0
        resize = self.resizeSpinBox.value()
        shape = np.array(im.shape)
        new_shape = (shape * resize/100.0).astype(int)
        scaled = transform.resize(im, new_shape).astype(np.float)
        imProcess = self.imageProcessSpinBox.value()
        im = gaussian_filter(scaled, imProcess)
        self.previewWindow.show()
        self.sendPreview.emit(scaled)

    def analyseMSD(self, tracks=None):
        """
        Perform a powerlaw fit of the MSD data.

        Args:
            tracks (pd.DataFrame, optional): The track data to be analyzed, uses
                stored tracks if None. Defaults to None.

        Returns:
            pd.DataFrame: A table containing the slopes and intercepts of the
                powerlaw fit.
        """
        if tracks is None:
            t = self.tracks
        else:
            t = tracks
        voxel = self.voxelSpinBox.value()
        fps = 1. / self.timestepSpinBox.value()
        im = tp.imsd(t, voxel, fps)
        res = fit_powerlaw(im, plot=False)
        return res

    def analyseTracks(self):
        """
        This function processes the particle tracks obtained from the previous
        step.

        In order to calculate relevant information such as radial and balistic
        scores, the center of mass (com) of all tracks will be calculated. There
        are three different options to calculate the track com:

        1: use the individual com of each track 
        2: use the center based on the enclosing circle for each track 
        3: use the initial positon of each track

        The com is then calculated via the minimal enclosing circle.

        :Note: The respective mode has to be enabled manually in the code since
        this part mainly affects the computation speed and not necessarily the
        precision.
        """
        self.analysisChanged = False
        self.updateTrackAnalysis.setStyleSheet("")
        if self.tracks is None:
            QtWidgets.QMessageBox.information(self, "Information", "Please track particles first")
            self.tabs.setCurrentIndex(0)
            self.trackButton.setStyleSheet("background-color: rgb(100, 200, 100)")
            self.trackAndIdentifyButton.setStyleSheet("background-color: rgb(100, 200, 100)")
            return None

        self.sendStatus.emit("Calculating center of mass")
        self.progressDialog.show()
        tc = self.lengthSpinBox.value()
        t = tp.filter_stubs(self.pos, tc)

        self.num = t["particle"].unique()
        tmax = len(self.num)
        indiv_com = np.zeros((tmax, 2))
        for i, j in enumerate(self.num):
            if self.actionCanceled:
                return None
            p = int((i + 1) / tmax * 100.0)
            self.sendProgress.emit(p)

            # No. 1: use center of mass of each particle for ensemble com
            indiv_com[i, :] = np.mean(t[t["particle"] == j][["x", "y"]].to_numpy(), axis=0)
            
            # No. 2: use center of enclosing circle of each track for ensemble com
            # indiv_com[i, :] = np.array(make_circle(t[t["particle"] == j][["x", "y"]].to_numpy()))[:2]
            
            # No. 3: use the initial position of each particle for ensemble com
            # indiv_com[i, :] = t[(t["particle"] == j)].iloc[0][["x", "y"]].to_numpy()
        com = np.array(make_circle(indiv_com))
        self.radius = com[2]
        com = np.array(com[:2])
        self.t = t

        self.msdData = self.analyseMSD(t)
        self.sendFilteredTracks.emit(t, com, indiv_com, self.msdData)

    def saveTrackButtonClicked(self):
        """
        Export the track view to file.
        """
        if self.plotReady:
            filters = "png (*.png);; svg (*.svg);; pdf (*.pdf)"
            fname = MyFileDialog.getSaveFileName(self, "Chose file name", ".", filters)[0]
            self.trackView.figure.savefig(fname, dpi=300)

    def saveBalisticButtonClicked(self):
        """
        Export the histogram for the balistic score to file.
        """
        if self.plotReady:
            filters = "png (*.png);; svg (*.svg);; pdf (*.pdf)"
            fname = MyFileDialog.getSaveFileName(self, "Chose file name", ".", filters)[0]
            self.analysisHist_1.figure.savefig(fname, dpi=300)

    def saveRadialButtonClicked(self):
        """
        Export the histogram for the radial score ti file.
        """
        if self.plotReady:
            filters = "png (*.png);; svg (*.svg);; pdf (*.pdf)"
            fname = MyFileDialog.getSaveFileName(self, "Chose file name", ".", filters)[0]
            self.analysisHist_2.figure.savefig(fname, dpi=300)

    def saveDistanceButtonClicked(self):
        """
        Export the histogram for the distance to com to file.
        """
        if self.plotReady:
            filters = "png (*.png);; svg (*.svg);; pdf (*.pdf)"
            fname = MyFileDialog.getSaveFileName(self, "Chose file name", ".", filters)[0]
            self.analysisHist_3.figure.savefig(fname, dpi=300)

    def saveMSDButtonClicked(self):
        """
        Export the histogram for the MSD slope and intercept to file.
        """
        if self.plotReady:
            filters = "png (*.png);; svg (*.svg);; pdf (*.pdf)"
            fname = MyFileDialog.getSaveFileName(self, "Chose file name", ".", filters)[0]
            basename = fname[:-4]
            ext = fname[-4:]
            self.analysisHist_4.figure.savefig(basename + "_slope" + ext, dpi=300)
            self.analysisHist_5.figure.savefig(basename + "_intercept" + ext, dpi=300)

    def saveAllButtonClicked(self):
        """
        Export all plots to file
        """
        if self.plotReady:
            filters = "png (*.png);; svg (*.svg);; pdf (*.pdf)"
            fname = MyFileDialog.getSaveFileName(self, "Chose file name", ".", filters)[0]
            images = [self.trackView, self.analysisHist_1, self.analysisHist_2, self.analysisHist_3, self.analysisHist_4, self.analysisHist_5]
            names = ["track", "balistic", "radial", "distance", "msd_slope", "msd_intercept"]
            basename = fname[:-4]
            ext = fname[-4:]
            for name in names:
                if os.path.exists(basename + "_" + name + ext):
                    reply = QtWidgets.QMessageBox.question(self, "Warning", "One or more files with that name already exists. Overwrite?", QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
                    if reply == QtWidgets.QMessageBox.No:
                        return None
                    else:
                        break
            self.progressDialog.show()
            self.sendStatus.emit("Saving...")
            total = len(names)
            progress = int(100 / total)
            for i, (image, name) in enumerate(zip(images, names)):
                if self.actionCanceled:
                    return None
                savename = basename + "_" + name + ext
                image.figure.savefig(savename, dpi=300)
                self.sendProgress.emit((i + 1)*progress)
            self.sendProgress.emit(100)
            self.progressDialog.accepted.emit()

    def saveTrackData(self):
        """
        Export all raw data to file
        """
        if self.msdData is None:
            QtWidgets.QMessageBox.information(self, "Information", "Please analyze tracks first")
            self.tabs.setCurrentWidget(self.tabAnalyze)
            self.updateTrackAnalysis.setStyleSheet("background-color: rgb(100, 200, 100)")
            return None
        if self.analysisChanged:
            reply = QtWidgets.QMessageBox.warning(self, "Warning", "The analysis parameters have changed. Do you want to re-run the track analysis?", QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.Yes)
            if reply == QtWidgets.QMessageBox.Yes:
                self.analyseTracks()
                self.saveTrackData()

        data = np.hstack((self.distanceData[:, None], self.balisticData[:, None], self.radialData[:, None], self.msdData))
        data = pd.DataFrame(data)
        data = data.rename(columns={0: "Distance from COM", 1: "Balsitic score", 2: "Radial score", 3: "MSD slope", 4: "MSD intercept"})
        filters = "csv (*.csv)"
        fname = MyFileDialog.getSaveFileName(self, "Chose file name", ".", filters)[0]
        if fname == "":
            return None
        fname = fname.replace(".csv", "")
        data.to_csv("{}_analysis.csv".format(fname))
        self.tracks.to_csv("{}_rawdata.csv".format(fname))

    @pyqtSlot(np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray)
    def plotTrackAnalysis(self, balistic, radial, distance, slope_grid, length_grid):
        """
        Generate the plots for all elements of the track analysis. 

        The function signature is for the proper matching of PyQt5 signals and
        slots. This is strictly speaking not required, but it helps debugging
        when new elements for plotting are added.

        Args:
            balistic (np.ndarray): Data with balistic scores, sorted by particle.
            radial (np.ndarray): Data with radial scores, sorted by particle.
            distance (np.ndarray): Data with distances from com, sorted by particle.
            slope_grid (np.ndarray): 2D Data with MSD slopes on an interpolated grid.
            length_grid (np.ndarray): 2D Data with track lenghts on an interpolated grid.
        """
        self.balisticData = balistic
        self.radialData = radial
        self.distanceData = distance

        b_threshold = self.balisticSpinBox.value()
        b_positive = balistic >= b_threshold
        r_threshold = self.radialSpinBox.value()
        r_positive = radial > r_threshold
        msdNData = self.msdData["n"]
        msdAData = self.msdData["A"]

        balistic_bins = np.linspace(balistic.min(), balistic.max(), 40)
        radial_bins = np.linspace(radial.min(), radial.max(), 40)
        distance_bins = np.linspace(distance.min(), distance.max(), 40)
        msd_n_bins = np.linspace(msdNData.min(), msdNData.max(), 20)
        msd_A_bins = np.linspace(msdAData.min(), msdAData.max(), 20)

        balistic_sets = [balistic[~b_positive], balistic[b_positive & r_positive], balistic[b_positive & ~r_positive]]
        radial_sets = [radial[~b_positive], radial[b_positive & r_positive], radial[b_positive & ~r_positive]]
        distance_sets = [distance[~b_positive], distance[b_positive & r_positive], distance[b_positive & ~r_positive]]
        msd_n_sets = [msdNData.to_numpy()[~b_positive], msdNData.to_numpy()[b_positive & r_positive], msdNData.to_numpy()[b_positive & ~r_positive]]
        msd_A_sets = [msdAData.to_numpy()[~b_positive], msdAData.to_numpy()[b_positive & r_positive], msdAData.to_numpy()[b_positive & ~r_positive]]

        self.sendStatus.emit("Plotting results")

        ax1 = self.trackView.axes
        ax2 = self.analysisHist_1.axes
        ax3 = self.analysisHist_2.axes
        ax4 = self.analysisHist_3.axes
        ax5 = self.analysisHist_4.axes
        ax6 = self.analysisHist_5.axes
        ax7 = self.analysisHist_6.axes
        ax8 = self.analysisHist_7.axes
        ax9 = self.analysisHist_8.axes

        self.resetAnalysisPlots(draw=False)

        colors = ["b", "g", "r"]

        if self.boxShowImage.isChecked():
            t = 0
            z = self.dataZScroll.value()
            c = self.dataCScroll.value()
            vmin = self.dataMinScroll.value()
            vmax = self.dataMaxScroll.value()
            if self.maxZProjection.isChecked():
                im = self.im[t, :, c, :, :].max(axis=0)
            else:
                im = self.im[t, z, c, :, :]*1
            # im[im < vmin] = 0
            # im[im > vmax] = vmax
            ax1.imshow(im, vmin=vmin, vmax=vmax, cmap="Greys")

        ax1.set_aspect(aspect=1.0)

        num = self.num
        t = self.t
        try:
            tp.plot_traj(t[t["particle"].isin(num[b_positive & r_positive])], ax=ax1, plot_style={"c": "g"})
        except ValueError:
            pass
        try:
            tp.plot_traj(t[t["particle"].isin(num[b_positive & ~r_positive])], ax=ax1, plot_style={"c": "r"})
        except ValueError:
            pass
        try:
            tp.plot_traj(t[t["particle"].isin(num[~b_positive])], ax=ax1, plot_style={"c": "b"})
        except ValueError:
            pass
        self.trackView.figure.tight_layout()
        self.trackView.draw()
        self.sendProgress.emit(17)

        ax2.hist(balistic_sets, bins=balistic_bins, stacked=True, color=colors)
        ax2.set_aspect(aspect="auto")
        ax2.set_xlim(0, 1.0)
        ax2.set_xlabel("Balistic score")
        ax2.set_ylabel("Count")
        self.analysisHist_1.figure.tight_layout()
        self.analysisHist_1.draw()
        self.sendProgress.emit(34)

        ax3.hist(radial_sets, bins=radial_bins, stacked=True, color=colors)
        ax3.set_aspect(aspect="auto")
        ax3.set_xlim(-0.6, 0.6)
        ax3.set_xlabel("Radial score")
        ax3.set_ylabel("Count")
        self.analysisHist_2.figure.tight_layout()
        self.analysisHist_2.draw()
        self.sendProgress.emit(50)

        ax4.hist(distance_sets, bins=distance_bins, stacked=True, color=colors)
        ax4.set_aspect(aspect="auto")
        xlim = ax4.get_xlim()
        ax4.set_xlim(0, xlim[1])
        ax4.set_xlabel("Distance from com [px]")
        ax4.set_ylabel("Count")
        self.analysisHist_3.figure.tight_layout()
        self.analysisHist_3.draw()
        self.sendProgress.emit(67)

        ax5.hist(msd_n_sets, bins=msd_n_bins, stacked=True, color=colors)
        ax5.set_aspect(aspect="auto")
        ax5.set_xlabel("MSD slope")
        ax5.set_ylabel("Count")
        self.analysisHist_4.figure.tight_layout()
        self.analysisHist_4.draw()
        self.sendProgress.emit(84)

        ax6.hist(msd_A_sets, bins=msd_A_bins, stacked=True, color=colors)
        ax6.set_aspect(aspect="auto")
        ax6.set_xlabel("MSD intercept")
        ax6.set_ylabel("Count")
        self.analysisHist_5.figure.tight_layout()
        self.analysisHist_5.draw()
        self.sendProgress.emit(100)

        ax7.set_aspect(aspect=1.0)
        print("grid mean:", slope_grid.mean())
        heatmap = ax7.imshow(slope_grid, norm=mpl_colors.DivergingNorm(slope_grid.mean()), cmap=plt.cm.seismic, interpolation="gaussian")
        plt.colorbar(heatmap, ax=ax7)
        ax7.set_xlabel("x")
        ax7.set_ylabel("y")
        self.analysisHist_6.figure.tight_layout()
        self.analysisHist_6.draw()

        ax8.set_aspect(aspect=1.0)
        print("grid mean:", length_grid.mean())
        heatmap = ax8.imshow(length_grid, norm=mpl_colors.DivergingNorm(length_grid.mean()), cmap=plt.cm.seismic, interpolation="gaussian")
        plt.colorbar(heatmap, ax=ax8)
        ax8.set_xlabel("x")
        ax8.set_ylabel("y")
        self.analysisHist_7.figure.tight_layout()
        self.analysisHist_7.draw()


        self.tabs.setCurrentWidget(self.tabAnalyze)
        self.progressDialog.accepted.emit()
        self.sendProgress.emit(0)
        self.plotReady = True

    def calculateCOM(self):
        """
        TODO: Check why parts of this are used in another function without
        calling this function. See: "analyseTracks"
        """
        s = len(self.pos)
        if s > 500:
            if self.tracks is None:
                i = np.random.choice(range(s), 100, replace=False)
                self.com = np.array(make_circle(self.pos[["x", "y"]].to_numpy()[i]))
            else:
                self.num = self.tracks["particle"].unique()
                tmax = len(self.num)
                initial_pos = np.zeros((tmax, 2))
                for i, j in enumerate(self.num):
                    p = int((i + 1) / tmax * 100.0)
                    self.sendProgress.emit(p)
                    # use center of mass of each particle for ensemble com
                    initial_pos[i, :] = np.mean(self.tracks[self.tracks["particle"] == j][["x", "y"]].to_numpy(), axis=0)
                    
                    # use center of enclosing circle of each track for ensemble com
                    # initial_pos[i, :] = np.array(make_circle(t[t["particle"] == j][["x", "y"]].to_numpy()))[:2]
                    
                    # use the initial position of each particle for ensemble com
                    # initial_pos[i, :] = t[(t["particle"] == j)].iloc[0][["x", "y"]].to_numpy()
                self.trackCenter = initial_pos
                self.com = np.array(make_circle(initial_pos))
        else:
            self.com = np.array(make_circle(self.pos[["x", "y"]].to_numpy()))

    def handleTrackingError(self, error):
        self.progressDialog.accepted.emit()
        self.sendProgress.emit(0)
        QtWidgets.QMessageBox.warning(self, "Warning!", "There was an error: {}".format(error))

    def loadTrackData(self, data, tracks):
        """
        Slot connected to signal from ParticleTracker::sendTracks

        Args:
            data (pd.DataFrame): The data containing the positions of identified
                particles
            tracks (bool): True if data contains tracked particle data, i.e.
                the tracking over time was performed.
        """
        if tracks:
            self.pos = data
            self.tracks = data
        else:
            self.pos = data
            self.tracks = None
            self.plotReady = False
        try:
            num = self.pos["particle"].unique()
            self.trackingResult.setText("Found {} unique particles across all frames".format(len(num)))
        except KeyError:
            self.trackingResult.setText("Please track particles to show unique particles")

        if self.pos is not None:
            self.calculateCOM()

        self.dataTScroll.setValue(int(0.2*self.dataTScroll.maximum()))
        self.progressDialog.updateProgress(0)
        self.progressDialog.accepted.emit()
        self.sendProgress.emit(0)
        t = self.dataTScroll.value()
        vmin = self.dataMinScroll.value()
        vmax = self.dataMaxScroll.value()
        self.showCurrentTracks(t, vmin, vmax)
        self.loadingTracksDone.emit()

    @pyqtSlot(int)
    def setTmax(self, tmax):
        self.loggingWidget.setTmax(tmax)

    def calcTracks(self):
        self.trackButton.setStyleSheet("")
        self.trackAndIdentifyButton.setStyleSheet("")
        self.identifyButton.setStyleSheet("")
        if self.im is None:
            QtWidgets.QMessageBox.information(self, "Information", "Please open an image first")
            self.tabs.setCurrentIndex(0)
            self.loadData.setStyleSheet("background-color: rgb(100, 200, 100)")
            self.loadFiles.setStyleSheet("background-color: rgb(100, 200, 100)")
            return None
        sender = self.sender()
        if sender == self.trackButton and self.pos is None:
            QtWidgets.QMessageBox.information(self, "Information", "Please identify particles first")
            self.tabs.setCurrentIndex(0)
            self.identifyButton.setStyleSheet("background-color: rgb(100, 200, 100)")
            self.trackAndIdentifyButton.setStyleSheet("background-color: rgb(100, 200, 100)")
            return None
        self.msdData = None
        self.progressDialog.show()
        z = self.dataZScroll.value()
        c = self.dataCScroll.value()
        if self.maxZProjection.isChecked():
            track_im = (self.im[:, :, c, :, :]).astype(np.float)
            track_im = track_im.max(axis=1)
        else:
            track_im = (self.im[:, z, c, :, :]).astype(np.float)
        self.trackParticleWrapper(sender, track_im)

    def getTrackingParameter(self):
        para = {
            "method": self.detectionMode.currentIndex(),
            "size": self.sizeSpinBox.value(),
            "sep": self.sepSpinBox.value(), 
            "iter": self.iterSpinBox.value(),
            "imProcess": self.imageProcessSpinBox.value(),
            "threshold": self.thresholdSpinBox.value(),
            "thresholder": self.thresholdMethod.currentText(),
            "distProcess": self.distanceProcessSpinBox.value(),
            "search": self.searchSpinBox.value(),
            "memory": self.memorySpinBox.value(),
            "resize": self.resizeSpinBox.value()
            }
        return para

    def trackParticleWrapper(self, sender, im):
        para = self.getTrackingParameter()
        if sender == self.trackButton:
            para["track"] = True
            self.trackParticles.emit(para)
        elif sender == self.identifyButton:
            para["track"] = False
            self.localizeParticles.emit(im, para)
        elif sender == self.trackAndIdentifyButton:
            para["track"] = True
            self.localizeParticles.emit(im, para)            

    @pyqtSlot(int)
    def dataTimeChanged(self, i):
        ignore(i)
        d = {
            self.dataTScroll: self.dataTValue,
            self.dataTValue: self.dataTScroll,

            self.dataZScroll: self.dataZValue,
            self.dataZValue: self.dataZScroll,

            self.dataCScroll: self.dataCValue,
            self.dataCValue: self.dataCScroll,

            self.dataMinScroll: self.dataMinValue,
            self.dataMinValue: self.dataMinScroll,

            self.dataMaxScroll: self.dataMaxValue,
            self.dataMaxValue: self.dataMaxScroll
        }
        sender = self.sender()
        if sender != self.colored:
            d[sender].blockSignals(True)
            d[sender].setValue(sender.value())
            d[sender].blockSignals(False)

        if self.im is None:
            return None
        t = self.dataTScroll.value()
        vmin = self.dataMinScroll.value()
        vmax = self.dataMaxScroll.value()
        if vmin >= vmax:
            if sender == self.dataMinScroll or self.dataMinValue:
                vmax = vmin + 2
                self.dataMaxScroll.setValue(vmax)
                self.dataMaxValue.setValue(vmax)
            elif sender == self.dataMaxScroll or sender == self.dataMaxValue:
                vmin = vmax - 2
                self.dataMinScroll.setValue(vmin)
                self.dataMinValue.setValue(vmin)
        if self.pos is not None and sender == self.dataTScroll:
            data = self.pos[self.pos["frame"] == t]
            num = len(data)
            self.currentTrackingResult.setText("Current frame: {} particles".format(num))

        self.showCurrentTracks(t, vmin, vmax)

    def resizeEvent(self, event):
        """Rescale some gui contents when resizing the window
        
        Arguments:
            event {[type]} -- [description]
        """
        ignore(event)
        # Resize the displayed image on the first tab
        if self.im is not None:
            t = self.dataTScroll.value()
            vmin = self.dataMinScroll.value()
            vmax = self.dataMaxScroll.value()
            self.showCurrentFile(t, vmin, vmax)
        # Resize the displayed image with identified particles on the second tab
        if self.im is not None:
            t = self.dataTScroll.value()
            vmin = self.dataMinScroll.value()
            vmax = self.dataMaxScroll.value()
            # self.showCurrentTracks(t, vmin, vmax)
        # Resize the analysis graphs on the third tab
        self.rescaleAnalysisPlots()

    def tabChanged(self, index):
        if self.im is not None:
            # if index == 0:
            #     t = self.dataTScroll.value()
            #     vmin = self.dataMinScroll.value()
            #     vmax = self.dataMaxScroll.value()
            #     self.showCurrentFile(t, vmin, vmax)
            # elif index == 1:
            t = self.dataTScroll.value()
            vmin = self.dataMinScroll.value()
            vmax = self.dataMaxScroll.value()
            self.showCurrentTracks(t, vmin, vmax)

    def rescaleAnalysisPlots(self):
        self.analysisHist_1.figure.tight_layout()
        self.analysisHist_1.draw()
        self.analysisHist_2.figure.tight_layout()
        self.analysisHist_2.draw()
        self.analysisHist_3.figure.tight_layout()
        self.analysisHist_3.draw()
        self.analysisHist_4.figure.tight_layout()
        self.analysisHist_4.draw()
        self.analysisHist_5.figure.tight_layout()
        self.analysisHist_5.draw()
        self.analysisHist_6.figure.tight_layout()
        self.analysisHist_6.draw()
        self.analysisHist_7.figure.tight_layout()
        self.analysisHist_7.draw()
        self.trackView.figure.tight_layout()
        self.trackView.draw()

    def showCurrentFile(self, i, vmin, vmax):
        t = self.dataTScroll.value()
        z = self.dataZScroll.value()
        c = self.dataCScroll.value()
        vmin = self.dataMinScroll.value()
        vmax = self.dataMaxScroll.value()
        if not self.colored.isChecked():
            im = (self.im[t, z, c, :, :]).astype(np.float)
            im[im > vmax] = vmax
            im -= vmin
            im[im < 0] = 0
            scale = 255.0/(im.max() + 1e-10)
            im *= scale
            images = [im, im, im]
        else:
            images = []
            for i in range(3):
                try:
                    im = (self.im[t, z, i, :, :]).astype(np.float)
                    im[im > vmax] = vmax
                    im -= vmin
                    im[im < 0] = 0
                    scale = 255.0/(im.max() + 1e-10)
                    im *= scale
                    images.append(im)
                except:
                    images.append(np.zeros(images[0].shape))
        rgbIm = np.stack(images, axis=2).astype(np.uint8)
        im_h, im_w = im.shape
        qim = QtGui.QImage(rgbIm.data, im_w, im_h, 3*im_w, QtGui.QImage.Format_RGB888)
        pix = QtGui.QPixmap(qim)
        w = self.currentFileView.width()
        h = self.currentFileView.height()
        pix = pix.scaled(w, h, QtCore.Qt.KeepAspectRatio)
        self.currentFileView.setPixmap(pix)

    @pyqtSlot(int)
    def trackTimeChanged(self, i):
        ignore(i)
        if self.im is None:
            return None

        d = {
            self.trackTimeScroll: self.trackTValue,
            self.trackTValue: self.trackTimeScroll,

            self.trackMinScroll: self.trackMinValue,
            self.trackMinValue: self.trackMinScroll,

            self.trackMaxScroll: self.trackMaxValue,
            self.trackMaxValue: self.trackMaxScroll
        }
        sender = self.sender()
        d[sender].blockSignals(True)
        d[sender].setValue(sender.value())
        d[sender].blockSignals(False)

        t = self.trackTimeScroll.value()
        vmin = self.trackMinScroll.value()
        vmax = self.trackMaxScroll.value()
        if vmin >= vmax:
            if sender == self.trackMinScroll or sender == self.trackMinValue:
                vmax = vmin + 2
                self.trackMaxScroll.setValue(vmax)
                self.trackMaxScroll.setValue(vmax)
            elif sender == self.trackMaxScroll or sender == self.trackMaxValue:
                vmin = vmax - 2
                self.trackMinScroll.setValue(vmin)
                self.trackMinValue.setValue(vmin)

        if self.pos is not None and sender == self.trackTimeScroll:
            data = self.pos[self.pos["frame"] == t]
            num = len(data)
            self.currentTrackingResult.setText("Current frame: {} particles".format(num))
        # old code that is temporarily not needed
        #     if self.boxShowHistograms.isChecked():
        #         for i in range(6):
        #             hist_data = data[data.columns[i + 2]].to_numpy()
        #             self.hists[i].set_hist_data(hist_data)
        #             # lims = self.histograms[i].plot.get_axis_limits(2)
        #             # self.histograms[i].plot.set_axis_limits(2, -0.1*lims[1], lims[1])

        self.showCurrentTracks(t, vmin, vmax)
        QtWidgets.QApplication.processEvents()

    def voronoiBoxChanged(self, state):
        self.voronoiOptionBox.setVisible(self.voronoiBox.isChecked())
        if self.pos is None:
            return None
        t = self.dataTScroll.value()
        vmin = self.dataMinScroll.value()
        vmax = self.dataMaxScroll.value()
        self.showCurrentTracks(t, vmin, vmax)

    def showCurrentTracks(self, i, vmin, vmax):
        t = self.dataTScroll.value()
        z = self.dataZScroll.value()
        c = self.dataCScroll.value()
        _, _, _, h, w = self.im.shape
        if not self.colored.isChecked():
            if self.maxZProjection.isChecked():
                im = (self.im[t, :, c, :, :]).astype(np.float)
                im = im.max(axis=0)
            else:
                im = (self.im[t, z, c, :, :]).astype(np.float)
            im[im > vmax] = vmax
            im -= vmin
            im[im < 0] = 0
            scale = 255.0/(im.max() + 1e-10)
            im *= scale
            images = [im, im, im]
        else:
            images = []
            for c in range(3):
                try:
                    if self.maxZProjection.isChecked():
                        im = (self.im[t, :, c, :, :]).astype(np.float)
                        im = im.max(axis=0)
                    else:
                        im = (self.im[t, z, c, :, :]).astype(np.float)
                    im[im > vmax] = vmax
                    im -= vmin
                    im[im < 0] = 0
                    scale = 255.0/(im.max() + 1e-10)
                    im *= scale
                    images.append(im)
                except:
                    images.append(np.zeros(images[0].shape))
        rgbIm = np.stack(images, axis=2).astype(np.uint8)
        qim = QtGui.QImage(rgbIm.data, w, h, 3*w, QtGui.QImage.Format_RGB888)

        if self.pos is not None:
            points = self.pos[self.pos["frame"] == i][["x", "y"]].to_numpy()
            radii = self.pos[self.pos["frame"] == i][["size"]].to_numpy()
            painter = QtGui.QPainter(qim)
            painter.setPen(QtCore.Qt.red)
            painter.setRenderHint(QtGui.QPainter.Antialiasing, True)

            if self.roiEdgesBox.isChecked() and "edge" in self.pos.columns:
                pen = painter.pen()
                penWidth = pen.width()
                pen.setWidth(2 * penWidth)
                painter.setPen(pen)
                edges = self.pos[self.pos["frame"] == i]["edge"].to_numpy()
                for edge in edges:
                    try:
                        edge = edge.astype(int)
                        polygon = QtGui.QPolygon()
                        polygon.setPoints(edge.flatten().tolist())
                        painter.drawPoints(polygon)
                    except:
                        pass
                pen.setWidth(penWidth)
                painter.setPen(pen)
            else:
                # draw circle around detected particles
                for p, r in zip(points, radii):
                    x0 = p - 1.5*r + 1
                    x, y = x0
                    painter.drawEllipse(int(x), int(y), int(3.0*r), int(3.0*r))
            
            # draw voronoi 
            if self.voronoiBox.isChecked():
                if self.voronoiOptionBox.isChecked() and self.com is not None:
                    vor = Voronoi(np.vstack((points, self.com[:2])))
                else:
                    vor = Voronoi(points)
                for i, p in enumerate(vor.points):
                    painter.drawText(*p, str(i))
                for i, v in enumerate(vor.vertices):
                    painter.drawText(*v, str(i))
                center = vor.points.mean(axis=0)
                ptp_bound = vor.points.ptp(axis=0)
                for pointidx, simplex in zip(vor.ridge_points, vor.ridge_vertices):
                    simplex = np.asarray(simplex)
                    if np.all(simplex >= 0):
                        i, j = simplex[0], simplex[1]
                        p0 = vor.vertices[i]
                        p1 = vor.vertices[j]
                        painter.drawLine(*p0, *p1)

                    else:
                        i = simplex[simplex >= 0][0]  # finite end Voronoi vertex
                        s = vor.vertices[i]

                        t = vor.points[pointidx[1]] - vor.points[pointidx[0]]  # tangent
                        t /= np.linalg.norm(t)
                        n = np.array([-t[1], t[0]])  # normal

                        midpoint = vor.points[pointidx].mean(axis=0)
                        direction = np.sign(np.dot(midpoint - center, n)) * n
                        far_point = s + direction * ptp_bound.max()
                        painter.drawLine(*s, *far_point)

            painter.end()

        pix = QtGui.QPixmap(qim)
        w = self.currentFileView.width()
        h = self.currentFileView.height()
        pix = pix.scaled(w, h, QtCore.Qt.KeepAspectRatio)
        self.currentFileView.setPixmap(pix)

    def fileSelected(self, item):
        path = item.text(1)
        self.progressDialog.show()
        self.sendStatus.emit("Reading image")
        self.sendImPath.emit(path)

    def setAnalysisValueChanged(self, val):
        self.analysisChanged = True

    def resetAnalysisPlots(self, draw=True):
        plots = [
            self.trackView,
            self.analysisHist_1,
            self.analysisHist_2,
            self.analysisHist_3,
            self.analysisHist_4,
            self.analysisHist_5,
            self.analysisHist_6,
            self.analysisHist_7,
            self.analysisHist_8,
        ]
        for p in plots:
            # first remove the colorbar of a plot before clearing it
            # clearing an axis clears the attribute "images" thus returning
            # no colorbar
            try:
                p.axes.images[-1].colorbar.remove()
            except (IndexError, AttributeError):
                pass
            p.axes.clear()
            if draw:
                p.draw()


    @pyqtSlot(np.ndarray)
    def processImage(self, im):
        # Reset stylesheet changes
        self.loadData.setStyleSheet("")
        self.loadFiles.setStyleSheet("")

        self.pos = None
        self.tracks = None
        self.msdData = None
        self.plotReady = False
        self.resetAnalysisPlots()

        self.im = im
        t, z, c, _y, _x = self.im.shape
        vmin, vmax = self.im[0].min(), self.im[0].max()
        try:
            med = int(np.mean(self.im[0, self.im[0] > vmin + 5]))
        except ValueError:
            med = vmax

        # Set all values for scrollbars
        self.dataTScroll.setMaximum(t - 1)
        self.dataZScroll.setMaximum(z - 1)
        self.dataCScroll.setMaximum(c - 1)
        self.dataMinScroll.setMaximum(vmax)
        self.dataMaxScroll.setMaximum(vmax)

        self.dataMinScroll.blockSignals(True)
        self.dataMinScroll.setValue(vmin + 5)
        self.dataMinScroll.blockSignals(False)

        self.dataMaxScroll.blockSignals(True)
        self.dataMaxScroll.setValue(med + 15)
        self.dataMaxScroll.blockSignals(False)       

        # set all values for spin boxes
        self.dataTValue.setMaximum(t - 1)
        self.dataZValue.setMaximum(z - 1)
        self.dataCValue.setMaximum(c - 1)
        self.dataMinValue.setMaximum(vmax)
        self.dataMaxValue.setMaximum(vmax)

        self.dataMinValue.blockSignals(True)
        self.dataMinValue.setValue(vmin + 5)
        self.dataMinValue.blockSignals(False)

        self.dataMaxValue.blockSignals(True)
        self.dataMaxValue.setValue(med + 15)
        self.dataMaxValue.blockSignals(False)       

        self.dataTScroll.setValue(int(0.2*t))
        t = self.dataTScroll.value()
        vmin = self.dataMinScroll.value()
        vmax = self.dataMaxScroll.value()
        self.showCurrentFile(t, vmin, vmax)

        self.progressDialog.accepted.emit()
        self.sendProgress.emit(0)

    def loadDataDir(self):
        self.loadData.setStyleSheet("")
        self.loadFiles.setStyleSheet("")
        self.trackButton.setStyleSheet("")
        path = ".data/"
        try:
            files = sorted(os.listdir(path))
        except FileNotFoundError:
            QtWidgets.QMessageBox.information(self, "Information", "Did not find \".data\" directory")
            return None
        for f in files:
            fname = os.path.abspath(path + f).replace(os.sep, "/")
            self.addFileToList(fname)
    
    def loadSelectedFiles(self):
        self.loadData.setStyleSheet("")
        self.loadFiles.setStyleSheet("")
        self.trackButton.setStyleSheet("")
        files = MyFileDialog.getOpenFileNames(self, "Open files", ".")[0]
        for f in files:
            fname = f.replace(os.sep, "/")
            self.addFileToList(fname)

    def addFileToList(self, fileName):
        if os.path.isfile(fileName):
            if len(self.fileList.findItems(fileName, QtCore.Qt.MatchExactly, 1)) == 0:
                if fileName.endswith(".tif") or fileName.endswith(".tiff"):
                    widget = QtWidgets.QTreeWidgetItem()
                    widget.setText(0, os.path.basename(fileName))
                    widget.setText(1, fileName)
                    self.fileList.addTopLevelItem(widget)
        else:
            for f in os.listdir(fileName):
                fname = os.path.abspath(fileName + f)
                self.addFileToList(fname)

    def dragEnterEvent(self, e):
        if e.mimeData().hasUrls:
            e.accept()
        else:
            e.ignore()

    def dragMoveEvent(self, e):
        if e.mimeData().hasUrls:
            e.accept()
        else:
            e.ignore()

    def dropEvent(self, e):
        if e.mimeData().hasUrls:
            e.setDropAction(QtCore.Qt.CopyAction)
            e.accept()
            # Workaround for OSx dragging and dropping
            for url in e.mimeData().urls():
                fname = str(url.toLocalFile())
                self.addFileToList(fname)
        else:
            e.ignore()

def main():
    app = QtWidgets.QApplication([])
    # app.aboutToQuit.connect(app.deleteLater)
    window = SingleParticleTracker([])
    # app.exec_()
    return window

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    window = SingleParticleTracker(sys.argv)
    window.show()
    app.exec_()
