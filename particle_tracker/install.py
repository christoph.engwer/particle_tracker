import os
import subprocess
import sys


def installRequirements(path, raiseError=True):
    """
    This function is called at the beginning to check for installed packages
    that are required by the single particle tracker
    
    Arguments:
        path {str} -- Path to requirements file
        raiseError {bool} -- raised Exception for missing packes (default: True)
    
    Returns:
        int -- Number of missing packages that could not be installed on-the-fly
    """
    print("Checking for requirements:\n")
    f = open(path)
    requirements = f.readlines()
    f.close()
    requirements = [r.replace("\n", "") for r in requirements]
    stdout = open("pip.list.tmp", "w")
    subprocess.check_call([sys.executable, "-m", "pip", "list"], stdout=stdout)
    stdout.close()
    f = open("pip.list.tmp", "r")
    installed = [line.split() for line in f.readlines()]
    installed = dict([[i[0].lower(), i[1]] for i in installed])
    f.close()
    os.remove("pip.list.tmp")

    missing = 0
    for package in requirements:
        delim = ["==", ">="]
        for d in delim:
            version = ""
            p = package.split(d)
            try:
                version = d + p[1]
                package = p[0]
                break
            except IndexError:
                pass
        print(package + version, end="\r")
        try:
            if not package.lower() in installed:
                print(package + version, u'\u231B', end="\r")
                subprocess.check_call([sys.executable, "-m", "pip", "install", "--user", package+version], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            print(package + version, u'\u2713')
        except subprocess.CalledProcessError:
            print(package + version, u'\u2717')
            missing += 1
    if missing > 0:
        message = "There are {} packages missing. Please install them manually and restart the application".format(missing)
        if raiseError:
            raise Exception(message)
        else:
            print(message)
    else:
        print("\nAll packages installed. Starting application... \n")
    return missing
