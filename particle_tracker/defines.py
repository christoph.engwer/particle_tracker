from skimage.filters import *
from enum import Enum, auto

__version__ = "0.7.0"

class IdentificationMethod(Enum):
    gaussianBlob = 0
    watershed = auto()

thresholds = dict({
                'Isodata': threshold_isodata,
                'Li': threshold_li,
                'Mean': threshold_mean,
                'Minimum': threshold_minimum,
                'Otsu': threshold_otsu,
                'Triangle': threshold_triangle,
                'Yen': threshold_yen,
                'Local': threshold_local
                })
