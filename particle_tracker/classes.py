import asyncio
import logging
import multiprocessing as mp
import os
import sys
from itertools import repeat
from time import sleep, time

import markdown2
import numpy as np
import pandas as pd
import pims
import tifffile
import trackpy as tp
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as Canvas
from matplotlib.figure import Figure
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from scipy.interpolate import griddata
from scipy.ndimage import gaussian_filter
from skimage import transform
from skimage.filters import *  # threshold_otsu, threshold_local, try_all_threshold

from .defines import *
from .defines import __version__
from .functions import *


class MatplotlibWidget(Canvas):
    """[summary]

    Args:
        parent (QWidget), optional): Parent QWidget. Defaults to None.
        title (str, optional): Plot title (currently unused). Defaults to ''.
        xlabel (str, optional): Label for x axis (currently unused). Defaults to ''.
        ylabel (str, optional): Label for y axis (currently unused). Defaults to ''.
        xlim ([type], optional): Limits for x axis (currently unused). Defaults to None.
        ylim ([type], optional): Limits for y axis (currently unused). Defaults to None.
        xscale (str, optional): Scale of x axis, either linear or log (currently unused). Defaults to 'linear'.
        yscale (str, optional): Scale of y axis, either linear or log (currently unused). Defaults to 'linear'.
        width (int, optional): Width of the figure in pixels. Defaults to 400.
        height (int, optional): Height of the figure in pixels. DefDefaults to 400.
        dpi (int, optional): Resolution of the figure in dpi. Defaults to 96.
        hold (bool, optional): Currently unused. Defaults to False.
    """
    def __init__(self, parent=None, title='', xlabel='', ylabel='',
                 xlim=None, ylim=None, xscale='linear', yscale='linear',
                 width=400, height=400, dpi=96, hold=False):
        super().__init__(Figure())
        self.figure = Figure(figsize=(width/dpi, height/dpi), dpi=dpi)
        self.canvas = Canvas(self.figure)
        self.axes = self.figure.add_subplot(111)

        Canvas.__init__(self, self.figure)
        self.setParent(parent)
        Canvas.setSizePolicy(self, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        Canvas.updateGeometry(self)

class MyHandler(object):
    """
    A simple handler for logging events. It runs in the listener process and
    dispatches events to loggers based on the name in the received record,
    which then get dispatched, by the logging system, to the handlers
    configured for those loggers.
    """
    def handle(self, record):
        logger = logging.getLogger()
        # The process name is transformed just to show that it's the listener
        # doing the logging to files and console
        record.processName = '%s (for %s)' % (mp.current_process().name, record.processName)
        logger.handle(record)

class ListNumpySignals(QtCore.QObject):
    """Custom class for some worker signals
    """
    finished = pyqtSignal()
    sendProgress = pyqtSignal(list, np.ndarray)

class NumpySignals(QtCore.QObject):
    """Custom class for some worker signals
    """
    finished = pyqtSignal()
    sendProgress = pyqtSignal(int, np.ndarray)

class PandasSignals(QtCore.QObject):
    """Custom class for some worker signals
    """
    finished = pyqtSignal()
    sendProgress = pyqtSignal(int, pd.DataFrame)

class QtWorker(QtCore.QRunnable):
    """Versatile worker for different tasks

    Args:
        func (python functon): A function that will be executed by the worker
        args (list): List of arguments that will be passed to "func"
        resDtype (dtype): Datatype of the result, e.g. numpy.ndarray, pandas.DataFrame, ...
        parent (QWidget, optional): Parent Widget. Defaults to None.
    """
    def __init__(self, func, args, resDtype, parent=None):
        super().__init__()
        self.func = func
        self.args = args
        self.res = None
        if resDtype == "array":
            self.signals = NumpySignals()
        elif resDtype == "listArray":
            self.signals = ListNumpySignals()
        elif resDtype == "dataframe":
            self.signals = PandasSignals()

    @pyqtSlot()
    def run(self):
        """Pass all arguments to the stored function and obtain its result.

        Emits a progress with sent arguments and the result to show progress
        on the user interface and to process the result.

        Emits finished once done.
        """
        self.res = self.func(*self.args[:-1])
        self.signals.sendProgress.emit(self.args[-1], self.res)
        self.signals.finished.emit()

class MyLogger(logging.Handler, QtCore.QObject):
    """Custom logging class that sends logging progress for tracking
    to the user interface.
    """
    sendProgress = pyqtSignal(int)
    stopTracking = pyqtSignal()

    def __init__(self, parent=None):
        logging.Handler.__init__(self)
        QtCore.QObject.__init__(self, parent)

        self.tmax = None
        self.actionCanceled = False
        logging.getLogger().addHandler(self)
        logging.getLogger().setLevel(logging.INFO)
        # listener = logging.handlers.QueueListener(mp.Queue(), MyHandler())
        # listener.start()

    def setTmax(self, tmax:int):
        """Set the maximum number of iterations a process is running.
        This is important to correctly emit a signal for the progress
        ranging from 0 to 100 %.

        Args:
            tmax (int): Maximum number of iteration for the logged process.
        """
        self.tmax = tmax

    def emit(self, record:logging.LogRecord):
        """[summary]

        Args:
            record (logging.LogRecord): [description]
        """
        if self.actionCanceled:
            self.stopTracking.emit()
        self.msg = record.args#self.format(record)
        if self.msg:
            p = self.msg[0]
        else:
            p = int(record.msg.split(" ")[1].replace(":", ""))
        try:
            p = int(round(p / (self.tmax - 1) * 100, 0))
            self.sendProgress.emit(p)
        except TypeError:
            pass

class ImageSampler(QtCore.QThread):
    sendProgress = pyqtSignal(int)
    done = pyqtSignal(int)
    sendResult = pyqtSignal(np.ndarray)

    def __init__(self, parent=None):
        super().__init__()
        self.im = None
        self.scaledIm = None
        self.alreadyDone = False

        self.pool = QtCore.QThreadPool.globalInstance()
        self.start()

    def sampleImages(self, im, shape, force=False):
        """
        Signal/Slot integration:
        
        SIGN: ParticleTracker.sampleImages
         |
         V
        SLOT: ImageSampler.sampleImages (THIS)
         |
         V
        SIGN (a): ImageSampler.sendResult
        SIGN (b): ImageSampler.done
         |
         V
        SLOT (a): ParticleTracker.handleSampledImage
        SLOT (b): ParticleTracker.newIdentifyParticles
        """
        # print(im.shape, shape)
        if im.shape[1:] == shape:
            # print("not sampling")
            self.sendResult.emit(im)
            self.done.emit(0)
        elif (self.im is not None and self.im.shape == im.shape and self.scaledIm.shape[1:] == shape) \
         and not force:
            # print("not sampling")
            self.sendResult.emit(self.scaledIm)
            self.done.emit(0)
        else:
            self.alreadyDone = False
            self.im = im
            tmax = self.im.shape[0]
            self.workerRes = np.zeros((tmax, *shape))
            self.poolProgress = 0
            self.poolProgressMax = tmax
            # print(self.pool.maxThreadCount())
            for i in range(tmax):
                worker = QtWorker(transform.resize, (self.im[i], shape, i), "array")
                worker.signals.sendProgress.connect(self.handleWorkerProgress)
                worker.signals.finished.connect(self.handleWorkerFinish)
                self.pool.start(worker)
    
    def handleWorkerProgress(self, i, res):
        self.workerRes[i] = res

    def handleWorkerFinish(self):
        self.poolProgress += 1
        try:
            p = int(round(self.poolProgress / (self.poolProgressMax - 1) * 100, 0))
        except ZeroDivisionError:
            p = 100
        self.sendProgress.emit(p)
        if self.poolProgress == self.poolProgressMax and not self.alreadyDone:
            self.alreadyDone = True
            self.scaledIm = self.workerRes
            self.sendResult.emit(self.workerRes)
            self.done.emit(0)

class ParticleIdentifier(QtCore.QThread):
    sendProgress = pyqtSignal(int)
    done = pyqtSignal(int)
    sendResult = pyqtSignal(pd.DataFrame)

    def __init__(self, parent=None):
        super().__init__()
        self.alreadyDone = False

        self.pool = QtCore.QThreadPool.globalInstance()
        self.start()

    def identifyParticles(self, frames, para):
        """
        Signal/Slot integration:
        
        SIGN: ParticleTracker.identifyParticlesSignal
         |
         V
        SLOT: ParticleIdentifier.identifyParticles (THIS)
        """
        self.alreadyDone = False

        resize = para["resize"]
        if para["method"] == IdentificationMethod.gaussianBlob.value:
            size = para["size"]
            threshold = para["threshold"]
            sep = para["sep"]
            iterations = para["iter"]
            size = int(size * resize/100.0) // 2 * 2 + 1
        else:
            imProcess = para["imProcess"]
            thresholder = para["thresholder"]
            distProcess = para["distProcess"]
        tmax = frames.shape[0]

        self.workerRes = [object]*tmax
        self.poolProgress = 0
        self.poolProgressMax = tmax
        for i in range(tmax):
            if para["method"] == IdentificationMethod.gaussianBlob.value:
                worker = QtWorker(myLocate, (frames[i], size, False, threshold, sep, iterations, i), "dataframe")
            else:
                worker = QtWorker(myWatershed, (frames[i], imProcess, thresholder, distProcess, i, i), "dataframe")
            worker.signals.sendProgress.connect(self.handleWorkerProgress)
            worker.signals.finished.connect(self.handleWorkerFinish)
            self.pool.start(worker)
 
    def handleWorkerProgress(self, i, res):
        self.workerRes[i] = res

    def handleWorkerFinish(self):
        """
        Signal/Slot integration:
        
        SIGN: QtWorker.signals.finished
         |
         V
        SLOT: ParticleIdentifier.handleWorkerFinish (THIS)
         |
         V
        SIGN (a): ImageSampler.sendResult
        SIGN (b): ImageSampler.done
         |
         V
        SLOT (a): ParticleTracker.handleParticleResults
        SLOT (b): ParticleTracker.newTrackParticles
        """
        self.poolProgress += 1
        try:
            p = int(round(self.poolProgress / (self.poolProgressMax - 1) * 100, 0))
        except ZeroDivisionError:
            p = 100
        self.sendProgress.emit(p)
        if self.poolProgress == self.poolProgressMax and not self.alreadyDone:
            self.alreadyDone = True
            for i, df in enumerate(self.workerRes):
                df["frame"] = i
            res = pd.concat(self.workerRes, ignore_index=True)
            self.sendResult.emit(res)
            self.done.emit(0)

class ParticleTracker(QtCore.QThread):
    """

    """
    sendTracks = pyqtSignal(pd.DataFrame, bool)
    sendSize = pyqtSignal(int)
    sendStatus = pyqtSignal(str)
    sendProgress = pyqtSignal(int)
    sampleImages = pyqtSignal(np.ndarray, tuple, bool)
    identifyParticlesSignal = pyqtSignal(np.ndarray, dict)
    sendTrackingError = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.frames = None
        self.framesScaled = None
        self.pos = None
        self.tracks = None
        self.actionCanceled = False
        self.logger = MyLogger()
        self.logger.stopTracking.connect(self.stopTracking)

        self.sampler = ImageSampler()
        self.sampleImages.connect(self.sampler.sampleImages)
        self.sampler.sendProgress.connect(self.handleSamplingProgress)
        self.sampler.sendResult.connect(self.handleSampledImage)
        self.sampler.done.connect(self.newIdentifyParticles)

        self.identifier = ParticleIdentifier()
        self.identifyParticlesSignal.connect(self.identifier.identifyParticles)
        self.identifier.sendProgress.connect(self.handleSamplingProgress)
        self.identifier.sendResult.connect(self.handleParticleResults)
        self.identifier.done.connect(self.newTrackParticles)
        self.start()

    def downsampleImages(self, resize):
        """TODO: UNUSED?"""
        t0 = time()
        self.sendStatus.emit("Downsampling images")
        tmax = len(self.frames)
        frames = None
        shape = np.array(self.frames[0].shape)
        new_shape = (shape * resize/100.0).astype(int)

        if self.framesScaled is not None and self.framesScaled.shape[1:] == tuple(new_shape):
            self.sendStatus.emit("Use same downscaled image")
            return None

        # if self.ipy_parallel:
        #     self.total = tmax
        #     myFunc = lambda image: transform.resize(image, new_shape)
        #     temp = self.frames*1
        #     amr = self.view.map_async(myFunc, temp)
        #     self.parallelProgress(amr)
        #     amr.wait()
        #     frames = np.array(amr.get())
        # else:
            # pool = mp.Pool(mp.cpu_count())
            # res = pool.starmap(myTransform, zip(self.frames, repeat(new_shape), range(tmax)))
            # # res = [None] * tmax
            # # for i in range(tmax):
            # #     res[i] = pool.apply_async(myTransform, args=(self.frames[i], new_shape, i), callback=myCallback).get()
            # pool.close()
            # pool.join()
            # frames = np.array(res)

        self.workerRes = np.zeros((tmax, *new_shape))
        pool = QtCore.QThreadPool()#.globalInstance()
        self.poolProgress = 0
        self.poolProgressMax = tmax
        print(pool.maxThreadCount())
        for i in range(tmax):
            worker = QtWorker(transform.resize, (self.frames[i], new_shape, i), "array")
            worker.signals.finished.connect(self.handleWorkerFinish)
            worker.signals.sendProgress.connect(self.handleWorkerProgress)
            pool.start(worker)
        done = pool.waitForDone()
        print(done)

            # for i in range(tmax):
            #     if self.actionCanceled:
            #         return None
            #     frame = transform.resize(self.frames[i], new_shape)
            #     p = int(round(i / (tmax - 1) * 100, 0))
            #     self.sendProgress.emit(p)
            #     try:
            #         frames[i] = frame
            #     except TypeError:
            #         frames = np.zeros((tmax, *(frame.shape)))
            #         frames[i] = frame

        t1 = time()
        print("\n\ntook: {}".format(t1 - t0))
        self.framesScaled = self.workerRes * 1
        return self.framesScaled

    def handleWorkerFinish(self):
        """TODO: UNUSED?"""
        self.poolProgress += 1
        p = int(round(self.poolProgress / (self.poolProgressMax - 1) * 100, 0))
        self.sendProgress.emit(p)
    
    def handleWorkerProgress(self, i, res):
        """TODO: UNUSED?"""
        self.workerRes[i] = res

    def stopTracking(self):
        """
        Signal/Slot integration:
        
        SIGN: MyLogger.stopTracking
         |
         V
        SLOT: ParticleTracker.stopTracking (THIS)
        """
        if self.actionCanceled:
            return None
        self.actionCanceled = True
        self.frames = None

    def identifyParticlesWrapper(self, im, para):
        """
        Signal/Slot integration:

        SIGN: SingleParticleTracker.localizeParticles
         |
         V
        SLOT: ParticleTracker.identifyParticlesWrapper (THIS)
         |
         V
        SIGN: ParticleTracker.sampleImages
         |
         V
        SLOT: ImageSampler.sampleImages
        """

        self.frames = im
        tmax = len(im)
        self.total = tmax
        self.logger.setTmax(tmax)
        self.para = para
        resize = para["resize"]
        shape = np.array(im.shape[1:])
        new_shape = tuple((shape * resize/100.0).astype(int))
        self.sendStatus.emit("Downsampling frames...")
        self.sampleImages.emit(im, new_shape, False)
    
    def handleParticleResults(self, data):
        """
        Signal/Slot integration:

        SIGN: ParticleIdentifier.sendResult
         |
         V
        SLOT: ParticleTracker.handleParticleResults (THIS)
        """
        self.pos = data
    
    def newTrackParticles(self, res):
        """
        Signal/Slot integration:

        SIGN: ParticleIdentifier.done
         |
         V
        SLOT: ParticleTracker.newTrackparticles (THIS)
         |
         V
        SIGN: ParticleTracker.sendTracks
         |
         V
        SLOT: SingleParticleTracker.loadTrackData
        """
        self.identifier.pool.waitForDone()
        if self.para["resize"] != 100:
            scale = 100 / self.para["resize"]
            self.pos[["x", "y", "size"]] *= scale
            if self.para["method"] == IdentificationMethod.watershed.value:
                self.pos["edge"] *= scale
        if self.para["track"]:
            self.trackParticles(self.para)
        else:
            self.sendTracks.emit(self.pos, False)

    def handleSampledImage(self, im):
        """
        Signal/Slot integration:
        
        SIGN: ImageSampler.sendresult
         |
         V
        SLOT: ParticleTracker.handleSampledImage (THIS)
        """
        self.framesScaled = im
    
    def handleSamplingProgress(self, p):
        """
        Signal/Slot integration:

        SIGN: ImageSampler.sendProgress
        SIGN: ParticleIdentifier.sendProgress
         |
         V
        SLOT: ParticleTracker.handleSamplingProgress (THIS)
         |
         V
        SIGN: ParticleTracker.sendProgress
         |
         V
        SLOT: MyProgress.updateProgress

        Forward the progress of the ImageSampler and the ParticleIdentifier to
        the progress dialog
        """
        self.sendProgress.emit(p)
    
    def newIdentifyParticles(self, res):
        """
        Signal/Slot integration:

        SIGN: ImageSampler.done
         |
         V
        SLOT: ParticleTracker.newIdentifyParticles (THIS)
         |
         V
        SIGN (a): ParticleTracker.sendStatus
        SIGN (b): ParticleTracker.identifyParticlesSignal
         |
         V
        SLOT (a): MyProgress.updateStatus
        SLOT (a): ParticleIdentifier.identifyParticles
        """
        if res != 0:
            return None
        self.sendStatus.emit("Identifying particles...")
        self.identifyParticlesSignal.emit(self.framesScaled, self.para)

    @pyqtSlot(np.ndarray, dict, bool)
    def identifyParticles(self, im, para, track=True):
        """TODO: UNUSED?"""
        self.frames = im
        tmax = len(im)
        self.total = tmax
        self.logger.setTmax(tmax)

        size = para["size"]
        threshold = para["threshold"]
        sep = para["sep"]
        iterations = para["iter"]
        resize = para["resize"]

        frames = None
        if resize != 100:
            self.downsampleImages(resize)
            self.framesScaled = self.workerRes * 1
            frames = self.framesScaled
            # rescale the target size, round to nearest larger odd number
            size = int(size * resize/100.0) // 2 * 2 + 1
        else:
            frames = self.frames

        t0 = time()
        self.sendStatus.emit("Identifying particles")
        try:
            # if self.ipy_parallel:
            #     myFunc = lambda image: tp.locate(image, size, invert=False, threshold=threshold, separation=sep, max_iterations=iterations, engine="numba")
            #     amr = self.view.map_async(myFunc, frames)
            #     self.parallelProgress(amr)
            #     amr.wait()
            #     res = amr.get()
            #     for i, df in enumerate(res):
            #         df["frame"] = i
            #     self.pos = pd.concat(res, ignore_index=True)
            if 1 == 1:
                self.pos = tp.batch(frames, size, invert=False, threshold=threshold, separation=sep, max_iterations=iterations, engine="numba")
                # self.pos = myBatch(frames, size, threshold, sep, iterations)
            else:
                self.workerRes = np.array([object]*tmax)
                pool = QtCore.QThreadPool.globalInstance()
                self.poolProgress = 0
                self.poolProgressMax = tmax
                for i in range(tmax):
                    myFunc = lambda image: tp.locate(image, size, invert=False, threshold=threshold, separation=sep, max_iterations=iterations, engine="numba")
                    worker = QtWorker(myFunc, (frames[i], i), "dataframe")
                    # worker = QtWorker(myLocate, (frames[i], size, False, threshold, sep, iterations, i), "dataframe")
                    worker.signals.finished.connect(self.handleWorkerFinish)
                    worker.signals.sendProgress.connect(self.handleWorkerProgress)
                    pool.start(worker)
                pool.waitForDone()
                for i, df in enumerate(self.workerRes):
                    df["frame"] = i
                self.pos = pd.concat(self.workerRes, ignore_index=True)
        except Exception as e:
            print(e)
            # return None
        self.doStuff()
        if resize != 100:
            scale = 100 / resize
            self.pos[["x", "y", "size"]] *= scale
        if track:
            self.trackParticles(para)
        else:
            self.sendTracks.emit(self.pos, False)
        t1 = time()
        print("took: {}".format(t1 - t0))

    @pyqtSlot(dict)
    def trackParticles(self, para):
        if self.frames is None:
            return None
        tmax = len(self.frames)
        self.logger.setTmax(tmax)

        search = para["search"]
        memory = para["memory"]

        self.sendStatus.emit("Calculating tracks")
        try:
            my_pred = tp.predict.NearestVelocityPredict()
            self.tracks = my_pred.link_df(self.pos, search, memory=memory)
        except Exception as e:
            print(e)
            self.sendTrackingError.emit(str(e))
            return None

        self.sendTracks.emit(self.tracks, True)

class MyFileDialog(QtWidgets.QFileDialog):
    """
    Own file dialog class

    Can probably be reverted to normal QFileDialog
    """
    def __init__(self, *args):
        super().__init__(*args)
        self.setOption(self.DontUseNativeDialog, True)
        self.setFileMode(self.ExistingFiles)
        self.directoryEntered.connect(self.checkFiles)

    @pyqtSlot(str)
    def checkFiles(self, files):
        """
        currently unused
        """
        pass

class MyProgress(QtWidgets.QDialog):
    """
    Progress dialog to show current status and progress of background tasks,
    e.g. image loading, image downsampling, particle identification, ...
    """
    resetActionCanceled = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)

        path = os.path.dirname(os.path.abspath(__file__))
        uic.loadUi(path + "/ui/progress.ui", self)

        self.status = self.findChild(QtWidgets.QLabel, "status")
        self.progressBar = self.findChild(QtWidgets.QProgressBar, "progressBar")
        self.buttonBox = self.findChild(QtWidgets.QDialogButtonBox, "buttonBox")

        self.accepted.connect(self.hide)

        self.progressBar.setValue(0)

    def showEvent(self, event):
        self.resetActionCanceled.emit()

    @pyqtSlot(int)
    def updateProgress(self, i):
        """
        Signal/Slot integration:

        SIGN: object.sendProgress
         |
         V
        SLOT: MyProgress.updateProgress (THIS)

        Update the progress

        Args:
            i (int): Progress [0..100]
        """
        self.progressBar.setValue(i)
        QtWidgets.QApplication.processEvents()

    @pyqtSlot(str)
    def updateStatus(self, s):
        """
        Signal/Slot integration:

        SIGN: object.sendStatus
         |
         V
        SLOT: MyProgress.updateStatus (THIS)

        Update the status text on the progress dialog.

        Args:
            s (str): Status text
        """
        self.status.setText(s)
        QtWidgets.QApplication.processEvents()

class TrackAnalyser(QtCore.QThread):
    """
    Dedicated class to analyze the track results in a different thread to keep
    the user interface responsive.
    """
    sendStatus = pyqtSignal(str)
    sendProgress = pyqtSignal(int)
    sendTrackAnalysis = pyqtSignal(np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray)

    def __init__(self, parent=None):
        super().__init__(parent)

        self.actionCanceled = False
        
        self.start()

    @pyqtSlot(pd.DataFrame, np.ndarray, np.ndarray, pd.DataFrame)
    def analyseTracks(self, trackData, com, indiv_com, msdData):
        """
        Signal/Slot integration:

        SIGN: SingleParticleTracker.sendFilteredTracks
         |
         V
        SLOT: TrackAnalyser.analyseTracks (THIS)
         |
         V
        SIGN: TrackAnalyser.sendTrackAnalysis
         |
         V
        SLOT: SingleParticleTracker.plotTrackAnalysis
        
        Function is called by signal SingleParticleTracker.sendFilteredTracks

        Get all relevant data from particle tracks that can and have to be
        analysed. Calculates radial and balistic scores for each track based on
        the global center of mass. Generates heatmaps for the MSD powerlaw
        slopes and the track length.

        Args:
            trackData (pd.DataFrame): results from particle tracking
            com (np.ndarray): center of mass of all tracks, shape=(2,)
            indiv_com (np.ndarray): the center of mass of each track, shape=(n, 2)
            msdData (pd.DataFrame): fitting results from MSD powerlaw fit
        """
        self.sendStatus.emit("Analysing tracks")

        data = trackData.copy()
        data[["x", "y"]] -= com

        num = data["particle"].unique()
        tmax = len(num)
        balistic = np.zeros(tmax)
        radial = np.zeros(tmax)
        distance = np.zeros(tmax)
        track_length = np.zeros(tmax)

        for i, j in enumerate(num):
            if self.actionCanceled:
                # break the loop if Cancel was pressed
                break
            p = int((i + 1) / tmax * 100.0)
            self.sendProgress.emit(p)

            pos = data[data["particle"] == j][["x", "y"]].to_numpy()
            track_length[i] = len(pos)

            length = np.linalg.norm(pos, axis=1)
            dif_l = np.abs(np.diff(length))
            new_p1 = pos[:-1, :] / length[:-1, None] * length[1:, None]
            dif_new_p1 = pos[1:, :] - new_p1[:, :]
            dif_new_p1_l = np.linalg.norm(dif_new_p1, axis=1)
            radial[i] = np.mean((dif_l - dif_new_p1_l) / (dif_l + dif_new_p1_l))
            radial[np.isnan(radial)] = 0

            total_step = pos[-1, :] - pos[0, :]
            total = np.linalg.norm(total_step)
            steps = np.diff(pos, axis=0)
            length = np.linalg.norm(steps, axis=1)
            trajectory = np.sum(length)
            balistic[i] = total / trajectory # / len(steps)
            balistic[np.isnan(balistic)] = 0

            com_i = np.mean(pos, axis=0)
            distance[i] = np.linalg.norm(com_i)
        else:
            # only process results if the loop finished
            n = msdData["n"].to_numpy()
            x = indiv_com[:, 0]
            y = indiv_com[:, 1]

            slope_grid = self.createHeatmap(x, y, n)
            length_grid = self.createHeatmap(x, y, track_length)

            self.sendTrackAnalysis.emit(balistic, radial, distance, slope_grid, length_grid)

    def createHeatmap(self, x, y, data, delta=100):
        nans = np.isnan(data)
        x = x[~nans]
        y = y[~nans]
        data = data[~nans]

        xmin, xmax = x.min(), x.max()
        ymin, ymax = y.min(), y.max()
        dx = dy = delta
        # dy = dx
        xi = np.linspace(xmin, xmax, dx + 1)
        yi = np.linspace(ymin, ymax, dy + 1)
        X, Y = np.meshgrid(xi, yi)

        GRID_FILTER = 5.0

        grid = griddata((x, y), data, (X, Y), method="linear")
        mean = grid[~np.isnan(grid)].mean()
        # print("Mean MSD slope:", meanN)
        grid[np.isnan(grid)] = mean
        unfilteredGrid = grid * 1
        grid = gaussian_filter(grid, GRID_FILTER)

        filteredGrid = gaussian_filter(np.abs(unfilteredGrid - grid), GRID_FILTER)
        # plt.imshow(gaussian_filter(np.abs(unfilteredGrid - grid), GRID_FILTER))
        # plt.colorbar()
        # plt.show()
        return grid

class MyPreview(QtWidgets.QDialog):
    """
    Separate dialog to show a preview, e.g. of the downsampled image.
    """
    def __init__(self, parent=None):
        super().__init__(parent)

        path = os.path.dirname(os.path.abspath(__file__))
        uic.loadUi(path + "/ui/preview.ui", self)

        self.previewLabel = self.findChild(QtWidgets.QLabel, "previewLabel")

    @pyqtSlot(np.ndarray)
    def displayImage(self, im:np.ndarray):
        """
        Function called by signal SingleParticleTracker.sendPreview.

        Display the image data on the preview label.

        Args:
            im (np.ndarray): image data
        """
        scale = 255.0/(im.max() + 1e-10)
        im *= scale
        rgbIm = np.stack((im, im, im), axis=2).astype(np.uint8)
        im_h, im_w = im.shape
        qim = QtGui.QImage(rgbIm.data, im_w, im_h, 3*im_w, QtGui.QImage.Format_RGB888)
        pix = QtGui.QPixmap(qim)
        w = self.previewLabel.width()
        h = self.previewLabel.height()
        pix = pix.scaled(w, h, QtCore.Qt.KeepAspectRatio)
        self.previewLabel.setPixmap(pix)

class ThresholdPreview(QtWidgets.QDialog):
    """
    A window to test different threshold methods on the current image. Shows a
    3x3 grid with the original image and the result of eight different threshold
    methods.

    By clicking on either of the thresholded images,
    ThresholdPreview.eventFilter is triggered that emits the selected
    thresholder to the main thread.
    """
    sendThresholder = pyqtSignal(int)

    def __init__(self, parent=None):
        super().__init__(parent)

        path = os.path.dirname(os.path.abspath(__file__))
        uic.loadUi(path + "/ui/preview_threshold.ui", self)
        for ax in self.preview.figure.axes:
            ax.remove()
        self.installEventFilter(self)
        self.preview.installEventFilter(self)

    def eventFilter(self, o, e):
        """
        Handle clicking on the thresholded images and emit the selected method
        to the main thread.

        Arguments: 
            o {QtCore.QObject} -- QObject triggering the event filter
            e {QtCore.QObject} -- QEvent type

        Returns:
            bool -- False
        """
        if e.type() == QtCore.QEvent.MouseButtonPress:
            w = self.preview.width()
            h = self.preview.height()
            pos = QtGui.QCursor.pos()
            pos = self.preview.mapFromGlobal(pos)
            x = pos.x()
            y = pos.y()
            xlimits = [[0, 200], [200, 400],[400, 600]]
            ylimits = [[35, 230], [270, 465],[500, 700]]
            valid = 0
            for xi, xlim in enumerate(xlimits):
                if x >= xlim[0] and x <= xlim[1]:
                    valid = 1
                    break
            for yi, ylim in enumerate(ylimits):
                if y >= ylim[0] and y <= ylim[1]:
                    valid = 1
                    break
            if valid:
                meth = yi * 3 + xi
                if meth > 0:
                    self.sendThresholder.emit(meth - 1)
                    self.close()
        return False

    def showThresholds(self, im:np.ndarray):
        """
        Function is called in SingleParticleTracker.thresholdPreviewClicked,
        specifically called from signal
        SingleParticleTracker.sendThresholdPreview.

        Process all thresholding methods from defines.thresholds and show the
        result in individual plots.

        Args:
            im (np.ndarray): image data to test thresholder on
        """
        gridspec = {"wspace":0.025, "hspace":0.2, "left":0.0, "right":1.0, "top":0.95, "bottom":0.0}
        axes = self.preview.figure.subplots(ncols=3, nrows=3, gridspec_kw=gridspec)
        axes = axes.ravel()
        axes[0].imshow(im)
        axes[0].set_title("Original")
        for i, method in enumerate(thresholds):
            if method == "Local":
                threshold = thresholds[method](im, 51)
                # show = threshold
            else:
                threshold = thresholds[method](im)
            show = im >= threshold
            axes[i + 1].imshow(show)
            axes[i + 1].set_title(method)

        for ax in axes:
            ax.set_axis_off()

        self.preview.figure.tight_layout()
        self.preview.draw()
        self.preview.figure.set_facecolor("none")
        self.preview.figure.patch.set_alpha(0.0)

class MyImageReader(QtCore.QThread):
    """
    Individual class for reading images in a separate thread to keep the user
    interface responsive.
    """
    sendProgress = pyqtSignal(int)
    sendImage = pyqtSignal(np.ndarray)

    def __init__(self, parent=None):
        super().__init__(parent)

        self.actionCanceled = False

        self.start()

    def readImage(self, path:str):
        """
        Function connected to SingleParticleTracker.sendImPath

        Reads tif image in "path" and sends image data to main thread. Tif image must be in "TZCYX" format. Best compatibility with tif images exported with ImageJ.

        Args:
            path (str): Path to tif image
        """
        tif = tifffile.TiffFile(path)
        final_shape = [1, 1, 1, 1, 1]
        k = 0
        for i, j in enumerate("TZCYX"):
            index = tif.series[0].axes.find(j)
            if index >= 0:
                final_shape[i] = tif.series[0].shape[index]
        # im = tif.asarray()
        pages = len(tif.series[0].pages)
        shape = tif.pages[0].shape
        shape = (pages, *shape)
        prod_old = 1
        for i in shape:
            prod_old *= i
        prod_new = 1
        for i in final_shape:
            prod_new *= i

        if prod_old == prod_new:
            im = np.zeros(shape)
            for i, page in enumerate(tif.pages):
                if self.actionCanceled:
                    return None
                p = int((i + 1) / pages * 100.0)
                self.sendProgress.emit(p)
                im[i] = page.asarray()
            im = im.reshape(final_shape)
        else:
            im = tif.asarray(maxworkers=mp.cpu_count())

        self.sendProgress.emit(100)
        self.sendImage.emit(im)

class AboutDialog(QtWidgets.QDialog):
    """
    Show copyright information and current version of the Single Particle
    Tracker software.
    """
    def __init__(self, parent=None):
        super().__init__(parent)

        path = os.path.dirname(os.path.abspath(__file__))
        uic.loadUi(path + "/ui/about.ui", self)

        self.versionLabel = self.findChild(QtWidgets.QLabel, "versionLabel")

    def showEvent(self, event):
        self.versionLabel.setText("Version: {}".format(__version__))

class MarkdownViewer(QtWidgets.QDialog):
    """
    A generic Markdown Viewer that has a "path" attribute pointing to a markdown
    file. Update "MarkdownViewer.path" before showing the dialog with
    "MarkdownViewer.show".

    When the showEvent is triggered, the file in MarkdownViewer.path is loaded
    and converted into html text that is displayed on a label. The viewer
    supports fenced code blocks but unfortunately does not support code
    highlighting, yet.
    """
    def __init__(self, parent=None):
        super().__init__(parent)

        path = os.path.dirname(os.path.abspath(__file__))
        uic.loadUi(path + "/ui/markdownviewer.ui", self)

        self.path = None
        self.label = self.findChild(QtWidgets.QLabel, "label")

    def showEvent(self, event):
        """
        Triggered when "MarkdownViewer.show" is called. Loads the markdown file
        in "MarkdownViewer.path" or closes and returns None when file not
        present or not defined.
        """
        if self.path is None or self.path == "":
            self.close()
            return None
        title = self.path.split(".")[0]
        self.setWindowTitle(title)
        markdown = markdown2.markdown_path(self.path, extras=["fenced-code-blocks"])
        self.label.setText(markdown)
