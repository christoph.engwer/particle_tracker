import sys

from PyQt5 import QtWidgets, QtGui

from particle_tracker import SingleParticleTracker

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    splash = QtWidgets.QSplashScreen()
    splash.setPixmap(QtGui.QPixmap("particle_tracker/res/particle_tracker.png"))
    splash.show()
    window = SingleParticleTracker(sys.argv, config="spt.config")
    window.show()
    splash.finish(window)
    app.exec_()
