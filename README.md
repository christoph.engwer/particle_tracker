# Installation

Check out the most recent [releases](https://terminaltor:1000/christoph/particle_tracker/releases) 

Or clone the repository with: 

```
git clone git@terminaltor:christoph/particle_tracker.git
```

Install python requirements

```
cd particle_tracker
pip3 install --user -r requirements.txt
```

Installing the required python libraries may fail. In that case, install cython and guidata individually first.

```
pip3 install --user cython
pip3 install --user guidata

cd particle_tracker
pip3 install --user -r requirements.txt
```


Run particle tracker

```
python3 main_gui.py
```

# Usage

1. Load files (actually file paths) into the program:
   1. Load the default directory ".data"
   2. Load file(s) via "Load files"
   3. Drag and drop files on the gui
2. Double-click on files in the file list to load the data into the program
3. Explore the image on the first tab
   1. Scroll through fluorescence channels
   2. Scroll through Z-stacks
   3. Scroll through time series
4. Select fluorescence channel based on which tracking should be performed
5. Toggle "Use max z-projection" if tracking should be performed across all z-slices
6. Adjust parameters for identifying and tracking particles and run tracking
8. Analyse tracks

# Use case with sample data

1. Click on " Load ".data" " at the bottom of the window to load the sample data

![Step 0](doc/use_case/step_000_s.jpg)

2. Double-click on the sample data to load the image

![Step 1](doc/use_case/step_001_s.jpg)

3. The default settings are working for the sample data. Click " Identify and Track! " at the bottom of the window. Depending on your computers performance, particles are first localized and then tracked over time.

![Step 2](doc/use_case/step_002_s.jpg)

4. Alternatively, you can use watershed detection instead of gaussian blob detection to localize particles (e.g. for nuclei). First select the " Watershed " tab, then select an appropriate tresholder (Triangle for the sample data) by clicking on the corresponding plot in the preview window. Then click " Identify and Track! " at the bottom of the window. Watershed segmentation will take longer than gaussian blob detection. For watershed segmentation, you may enable " Plot ROI edges " to show the detected contours instead of centroid.

![Step 3](doc/use_case/step_003_s.jpg)

![Step 4](doc/use_case/step_004_s.jpg)

5. For the analysis, switch to the second tab of the application " Analyse tracks " and click on " Analyze/Update ".

![Step 5](doc/use_case/step_005_s.jpg)


# API

The Single Particle Tracker can be controlled via an API. These examples show the basic steps how to use the API:

## Load the API and create an instance of it

```python
from particle_tracker.api import SPTApi
api = SPTApi(config="spt.config")
```

## Register files and load them

```python
api.registerFiles(".data")
print(api.registeredFiles[0])
>>> 0_sample_data.tif
print(api.registeredFilePaths[0])
>>> C:/Users/User/Downloads/particle_tracker-master/.data/0_sample_data.tif
api.loadFile(0)
```

## Localize and track particles

```python
pos = api.apiLocateParticles()
print(pos.head(5))
>>>             y           x         mass      size       ecc     signal  raw_mass        ep  frame
... 0   73.303191  206.930851   385.835642  1.913001  0.072464  26.680124   13251.0  0.074974      0
... 1   93.190141  167.640845   291.429049  1.868946  0.169346  22.575490   13116.0  0.091255      0
... 2   98.945455  289.963636   112.877448  1.623688  0.155589  10.261586   12771.0  0.205047      0
... 3  100.880795  127.940397   309.899904  1.927494  0.139270  20.523172   13224.0  0.077748      0
... 4  109.186879  137.669980  1032.315574  2.111222  0.073046  57.464883   14656.0  0.026244      0

tracks = api.apiTrackParticles()
print(tracks.head(5))
>>>             y           x         mass      size       ecc     signal  raw_mass        ep  frame  particle
... 0   73.303191  206.930851   385.835642  1.913001  0.072464  26.680124   13251.0  0.074974      0         0
... 1   93.190141  167.640845   291.429049  1.868946  0.169346  22.575490   13116.0  0.091255      0         1
... 2   98.945455  289.963636   112.877448  1.623688  0.155589  10.261586   12771.0  0.205047      0         2
... 3  100.880795  127.940397   309.899904  1.927494  0.139270  20.523172   13224.0  0.077748      0         3
... 4  109.186879  137.669980  1032.315574  2.111222  0.073046  57.464883   14656.0  0.026244      0         4

tracks = api.apiIdentifyAndTrackParticles()
print(tracks.head(5))
>>>             y           x         mass      size       ecc     signal  raw_mass        ep  frame  particle
... 0   73.303191  206.930851   385.835642  1.913001  0.072464  26.680124   13251.0  0.074974      0         0
... 1   93.190141  167.640845   291.429049  1.868946  0.169346  22.575490   13116.0  0.091255      0         1
... 2   98.945455  289.963636   112.877448  1.623688  0.155589  10.261586   12771.0  0.205047      0         2
... 3  100.880795  127.940397   309.899904  1.927494  0.139270  20.523172   13224.0  0.077748      0         3
... 4  109.186879  137.669980  1032.315574  2.111222  0.073046  57.464883   14656.0  0.026244      0         4
```

## Analyze the track results

```python
api.apiAnalyseTracks()
api.apiExportAllPlots()
# This opens a file dialog and asks for a filename
api.apiExportAllData()
# This opens a file dialog and asks for a filename
```

